# **News managment** #
## **About app** ##
Web app contains three modules. 

**News-common**: service & dao implementations.

**News-admin**: controllers & views for administration part.

**News-client**: controllers & views for user part.
## **Branches** ##
**master** branch contains version of 2d task with Hibernate dao implemetation.

**second_task** branch contains version of 2d task with JDBC dao implemetation.

**hibernate_task** branch contains version of 2d task with Hibernate dao implementation. Currently in progress.
## **Deploy** ##
Task deploys on Tomcat server with the help of Maven plugin for Tomcat.

```
#!
mvn tomcat7:deploy

```