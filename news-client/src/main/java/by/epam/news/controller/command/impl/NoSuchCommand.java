package by.epam.news.controller.command.impl;

import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.constant.JspPages;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class NoSuchCommand implements Command {
    public String execute(HttpServletRequest request) throws CommandException {
        return JspPages.ERROR;
    }
}
