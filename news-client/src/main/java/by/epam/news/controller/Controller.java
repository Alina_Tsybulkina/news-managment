package by.epam.news.controller;

import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.command.CommandHelper;
import by.epam.news.controller.command.CommandNames;
import by.epam.news.controller.constant.JspPages;
import by.epam.news.controller.constant.ParameterName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet("/Controller")
public class Controller extends HttpServlet {
    
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    private final static String BEANS_DECLARATION_XML = "classpath*:beans-def.xml";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * The method is processing request from client and calls the command depending on parameter {@code command}.
     * Then forwards a request from a servlet to another resource.
     * @param request - object of class {@code HttpServletRequest}, stores an information from client.
     * @param response - object of class {@code HttpServletResponse}, stores an information from server.
     * @throws ServletException - exception a servlet can throw if {@code RequestDispatcher} is used improperly.
     * @throws IOException - exception a servlet can throw if {@code RequestDispatcher} is used improperly.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        String commandName;
        Command command;
        try {
            commandName = request.getParameter(ParameterName.COMMAND.name().toLowerCase());
            if (commandName == null){
                commandName = CommandNames.SHOW_ALL_NEWS.name().toLowerCase();
            }
            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(BEANS_DECLARATION_XML);
            CommandHelper commandHelper = ctx.getBean(CommandHelper.class);
            command = commandHelper.getCommand(commandName);
            page = command.execute(request);
        } catch (CommandException e) {
            page = JspPages.ERROR;
            LOGGER.error(e);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);

        if (dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }
}