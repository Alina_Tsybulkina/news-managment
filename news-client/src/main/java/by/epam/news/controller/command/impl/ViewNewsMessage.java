package by.epam.news.controller.command.impl;

import by.epam.alina.service.NewsService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.constant.JspPages;
import by.epam.news.controller.constant.ParameterName;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ViewNewsMessage implements Command {

    private NewsService newsService;

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public String execute(HttpServletRequest request) throws CommandException {
        Long currentId = Long.valueOf(request.getParameter(ParameterName.NEWS_ID.name().toLowerCase()));
        try {
            request.setAttribute(ParameterName.NEWS_DTO.name().toLowerCase(),
                    newsService.getOne(currentId));
            request.setAttribute(ParameterName.PREVIOUS_ID.name().toLowerCase(), newsService.getPrevious(currentId));
            request.setAttribute(ParameterName.NEXT_ID.name().toLowerCase(), newsService.getNext(currentId));
            request.setAttribute(ParameterName.PAGE.name().toLowerCase(), request.getParameter(ParameterName.PAGE.name().toLowerCase()));
        } catch (ServiceException e) {
            throw new CommandException("Unable to get news object.");
        }
        return JspPages.CLIENT_VIEW_NEWS;
    }
}
