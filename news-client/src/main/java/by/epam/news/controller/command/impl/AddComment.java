package by.epam.news.controller.command.impl;

import by.epam.alina.entity.Comment;
import by.epam.alina.service.CommentService;
import by.epam.alina.service.NewsService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.constant.JspPages;
import by.epam.news.controller.constant.ParameterName;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;

@Component
public class AddComment implements Command {

    private CommentService commentService;
    private NewsService newsService;

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Comment comment = new Comment();
        Long currentId = Long.parseLong(request.getParameter(ParameterName.NEWS_ID.name().toLowerCase()));
        comment.getNews().setNewsId(currentId);
        comment.setCreationDate(new Timestamp(new Date().getTime()));
        comment.setCommentText(request.getParameter(ParameterName.COMMENT_TEXT.name().toLowerCase()));
        try {
            commentService.addComment(comment);
            request.setAttribute(ParameterName.PREVIOUS_ID.name().toLowerCase(), newsService.getPrevious(currentId));
            request.setAttribute(ParameterName.NEXT_ID.name().toLowerCase(), newsService.getNext(currentId));
            request.setAttribute(ParameterName.NEWS_DTO.name().toLowerCase(), newsService.getOne(comment.getNews().getNewsId()));
            request.setAttribute(ParameterName.PAGE.name().toLowerCase(), request.getParameter(ParameterName.PAGE.name().toLowerCase()));
        } catch (ServiceException e) {
            throw new CommandException("Unable to add comment to the current news message.");
        }
        return JspPages.CLIENT_VIEW_NEWS;
    }
}
