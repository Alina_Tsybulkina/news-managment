package by.epam.news.controller.command.impl;

import by.epam.alina.entity.Author;
import by.epam.alina.entity.dto.SearchNewsDTO;
import by.epam.alina.service.AuthorService;
import by.epam.alina.service.NewsService;
import by.epam.alina.service.TagService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.command.CommandNames;
import by.epam.news.controller.constant.JspPages;
import by.epam.news.controller.constant.ParameterName;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

@Component
public class SearchNews implements Command {

    private NewsService newsService;
    private AuthorService authorService;
    private TagService tagService;

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public String execute(HttpServletRequest request) throws CommandException {
        SearchNewsDTO searchNewsDTO = new SearchNewsDTO();
        searchNewsDTO.setAuthor(new Author());
        searchNewsDTO.getAuthor().setAuthorId(Long.parseLong(request.getParameter(ParameterName.AUTHORS_SELECT.name().toLowerCase())));
        String[] tagsId = request.getParameterValues(ParameterName.TAGS_SELECT.name().toLowerCase());
        if (tagsId != null) {
            Set<Long> tagsSet = new HashSet<Long>(tagsId.length);
            for (String id : tagsId) {
                tagsSet.add(Long.parseLong(id));
            }
            searchNewsDTO.setTagsId(tagsSet);
        }
        try {
            request.setAttribute(ParameterName.NEWS_DTOS.name().toLowerCase(), newsService.searchNews(searchNewsDTO));
            request.setAttribute(ParameterName.TAGS.name().toLowerCase(), tagService.getAll());
            request.setAttribute(ParameterName.AUTHORS.name().toLowerCase(), authorService.getAll());
            request.setAttribute(ParameterName.COMMAND.name().toLowerCase(), CommandNames.SEARCH_NEWS.name().toLowerCase());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return JspPages.CLIENT_NEWS_LIST;
    }
}
