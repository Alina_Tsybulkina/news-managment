package by.epam.news.controller.command;

import by.epam.news.controller.command.impl.*;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.HashMap;
import java.util.Map;

/**
 * Class which stores a map with commands objects. Singleton.
 */
@Configurable
public final class CommandHelper {

    private Map<CommandNames, Command> commands = new HashMap<>();

    public CommandHelper(AddComment addComment, SearchNews searchNews, ShowAllNews showAllNews, ViewNewsMessage viewNewsMessage, NoSuchCommand noSuchCommand) {
        commands.put(CommandNames.ADD_COMMENT, addComment);
        commands.put(CommandNames.SEARCH_NEWS, searchNews);
        commands.put(CommandNames.SHOW_ALL_NEWS, showAllNews);
        commands.put(CommandNames.VIEW_NEWS_MESSAGE, viewNewsMessage);
        commands.put(CommandNames.NO_SUCH_COMMAND, noSuchCommand);
    }

    /**
     * Method which returns object of command from map, depending on parameter.
     * @param commandName - value of parameter {@code command} from request.
     * @return - command object.
     */
    public Command getCommand(String commandName)
    {
        CommandNames name = CommandNames.valueOf(commandName.toUpperCase());
        Command command;
        if (name != null)
        {
            command = commands.get(name);
        }
        else
        {
            command = commands.get(CommandNames.NO_SUCH_COMMAND);
        }
        return command;
    }
}
