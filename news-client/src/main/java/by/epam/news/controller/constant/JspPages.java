package by.epam.news.controller.constant;

/**
 * Final class which contains names of jsp pages.
 */
public final class JspPages {
    public static final String CLIENT_NEWS_LIST = "client_news_list.tiles";
    public static final String CLIENT_VIEW_NEWS = "client_view_news.tiles";
    public static final String ERROR = "error.tiles";
}
