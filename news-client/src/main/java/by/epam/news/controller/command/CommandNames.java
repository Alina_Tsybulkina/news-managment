package by.epam.news.controller.command;

public enum CommandNames {
    SEARCH_NEWS, VIEW_NEWS_MESSAGE, SHOW_ALL_NEWS, ADD_COMMENT, NO_SUCH_COMMAND
}
