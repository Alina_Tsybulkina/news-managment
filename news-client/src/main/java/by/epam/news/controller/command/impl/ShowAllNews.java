package by.epam.news.controller.command.impl;

import by.epam.alina.service.AuthorService;
import by.epam.alina.service.NewsService;
import by.epam.alina.service.TagService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.news.controller.command.Command;
import by.epam.news.controller.command.CommandException;
import by.epam.news.controller.command.CommandNames;
import by.epam.news.controller.constant.JspPages;
import by.epam.news.controller.constant.ParameterName;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

@Component
public class ShowAllNews implements Command {

    private NewsService newsService;
    private AuthorService authorService;
    private TagService tagService;

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        int page = 1;
        if (request.getParameter(ParameterName.PAGE.name().toLowerCase()) != null) {
            page = Integer.parseInt(request.getParameter(ParameterName.PAGE.name().toLowerCase()));
        }
        try {
            request.setAttribute(ParameterName.NEWS_DTOS.name().toLowerCase(), newsService.getSortedNews(page));
            long newsNumber = newsService.countNews();
            request.setAttribute(ParameterName.PAGES_NUMBER.name().toLowerCase(), newsNumber % 3 == 0? newsNumber/3 : newsNumber/3 + 1);
            request.setAttribute(ParameterName.TAGS.name().toLowerCase(), tagService.getAll());
            request.setAttribute(ParameterName.AUTHORS.name().toLowerCase(), authorService.getAll());
            request.setAttribute(ParameterName.COMMAND.name().toLowerCase(), CommandNames.SHOW_ALL_NEWS.name().toLowerCase());
            request.setAttribute(ParameterName.PAGE.name().toLowerCase(), page);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return JspPages.CLIENT_NEWS_LIST;
    }

}
