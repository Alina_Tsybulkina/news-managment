<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="./resource/client_module.css">
</head>
<body>
<header>
    <tiles:insertAttribute name="header"/>
</header>
<main>
    <tiles:insertAttribute name="main"/>
</main>
<footer>
    <tiles:insertAttribute name="footer"/>
</footer>
</body>

</html>