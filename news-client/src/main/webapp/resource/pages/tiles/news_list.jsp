<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<br>
<div class="news_list">
    <form method="post" action="Controller">
        <select name="authors_select">
            <c:forEach items="${authors}" var="author">
                <option value="${author.authorId}">${author.authorName}</option>
            </c:forEach>
        </select>
        <select multiple="multiple" name="tags_select">
            <c:forEach items="${tags}" var="tag">
                <option value="${tag.tagId}">${tag.tagName}</option>
            </c:forEach>
        </select>
        <br><br>
        <input type="hidden" name="command" value="search_news">
        <button type="submit">Search</button>
    </form>
    <br>
    <c:forEach items="${news_dtos}" var="newsDTO" varStatus="loop">
        <div class="news_message">
            <form method="post" action="Controller">
                <input type="hidden" name="command" value="view_news_message">
                <input type="hidden" name="page" value="${page}">
                <input type="hidden" name="news_id" value="${newsDTO.newsId}"/>
                <button class="button_as_link">${newsDTO.title}</button>
            </form>
            <span class="gray_text">Author(s): </span>
            <c:forEach items="${newsDTO.authors}" var="author" varStatus="status">
                <span class="gray_text">${author.authorName}</span>
                <c:if test="${ not (status.last)}"><span class="gray_text">,</span></c:if>
            </c:forEach><br>

            <span class="gray_text">Tag(s): </span>
            <c:forEach items="${newsDTO.tags}" var="tag" varStatus="status">
                <span class="gray_text">${tag.tagName}</span>
                <c:if test="${ not (status.last)}"><span class="gray_text">,</span></c:if>
            </c:forEach><br>

            <span>Created: <i>${newsDTO.creationDate}</i></span>
            <span>Last modified: <i>${newsDTO.modificationDate}</i></span><br>

            <p>${newsDTO.shortText}</p>
            <span> Comments(${newsDTO.comments.size()})</span><br>
            <br><br>
        </div>
    </c:forEach>

    <c:if test="${not empty pages_number}">
        <div class="pages">
            <c:forEach begin="1" end="${pages_number}" varStatus="iteration">
                <form class="inline_form" method="post" action="Controller">
                    <input type="hidden" name="command" value="${command}"/>
                    <input type="hidden" value="${iteration.index}" name="page">
                    <button id="${iteration.index}" type="submit">${iteration.index}</button>
                </form>
            </c:forEach>
        </div>
    </c:if>

</div>

