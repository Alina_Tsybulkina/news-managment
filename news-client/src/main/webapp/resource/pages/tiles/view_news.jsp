<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<form class="back_button" action="Controller" method="get">
    <c:if test="${page == null}">
        <input type="hidden" name="page" value="1">
    </c:if>
    <c:if test="${not page == null}">
        <input type="hidden" name="page" value="${page}">
    </c:if>
    <input class="button_as_link" type="submit" value="< Back">
</form>
<div class="full_news_message">

    <h1>${news_dto.title}</h1>

    <span class="gray_text">Author(s): </span>
    <c:forEach items="${news_dto.authors}" var="author" varStatus="status">
        <span class="gray_text">${author.authorName}</span>
        <c:if test="${ not (status.last)}"><span class="gray_text">,</span></c:if>
    </c:forEach><br>
    <span class="gray_text">Tag(s): </span>
    <c:forEach items="${news_dto.tags}" var="tag" varStatus="status">
        <span class="gray_text">${tag.tagName}</span>
        <c:if test="${ not (status.last)}"><span class="gray_text">,</span></c:if>
    </c:forEach><br>

    <span>Created: <i>${news_dto.creationDate}</i></span>
    <span>Last modified: <i>${news_dto.modificationDate}</i></span>
    <p>${news_dto.fullText}</p>
    <c:forEach items="${news_dto.comments}" var="comment">
        <span class="underline_text">${comment.creationDate}</span>
        <p class="comment_text">${comment.commentText}</p>
    </c:forEach>

    <form method="post" action="Controller">
        <textarea name="comment_text" placeholder="Enter your comment..."></textarea>
        <input type="hidden" name="page" value="${page}">
        <input type="hidden" name="command" value="add_comment">
        <input type="hidden" name="news_id" value="${news_dto.newsId}"/><br>
        <button>Add comment</button>
    </form>

</div>

<div class="next_prev_navigation">
    <c:if test="${not ((previous_id == 0) || (previous_id == null))}">
        <form class="previous_button" method="post" action="Controller">
            <input type="hidden" name="page" value="${page}">
            <input type="hidden" name="command" value="view_news_message">
            <input type="hidden" name="news_id" value="${previous_id}">
            <input type="submit" class="button_as_link" value="< Previous">
        </form>
    </c:if>

    <c:if test="${not ((next_id == 0) || (next_id == null))}">
        <form class="next_button" method="post" action="Controller">
            <input type="hidden" name="page" value="${page}">
            <input type="hidden" name="command" value="view_news_message">
            <input type="hidden" name="news_id" value="${next_id}">
            <input type="submit" class="button_as_link" value="Next >">
        </form>
    </c:if>
</div>
