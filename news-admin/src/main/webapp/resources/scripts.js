function show(form) {
    form.getElementsByClassName('name_input')[0].disabled = false;
    form.getElementsByClassName('edit')[0].setAttribute('onclick', 'hide(this.parentNode)');
    form.getElementsByClassName('edit')[0].setAttribute('value', 'Cancel');
    for (var i = 0; i < form.getElementsByClassName('edit_buttons').length; i++) {
        form.getElementsByClassName('edit_buttons')[i].style.visibility = 'visible';
    }
}

function hide(form) {
    form.getElementsByClassName('name_input')[0].disabled = true;
    form.getElementsByClassName('edit')[0].setAttribute('onclick', 'show(this.parentNode)');
    form.getElementsByClassName('edit')[0].setAttribute('value', 'Edit');
    for (var i = 0; i < form.getElementsByClassName('edit_buttons').length; i++) {
        form.getElementsByClassName('edit_buttons')[i].style.visibility = 'hidden';
    }
}

function checkNews() {
    var regexShortText = "[\D]{3,100}";
    var regexFullText = "[\D]{3,2000}";
    if (! document.getElementsByName('shortText')[0].value.match(regexShortText)){
        alert('Short text size should be within 3 and 100 symbols');
        return false;
    }
    if (! document.getElementsByName('fullText')[0].value.match(regexFullText)){
        alert('Full text size should be within 3 and 2000 symbols');
        return false;
    }
}