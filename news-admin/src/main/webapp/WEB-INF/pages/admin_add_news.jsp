<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form cssClass="add_news_form" action="add_news_message.do" method="post" modelAttribute="AddNewsDTO">
    <br>
    <form:errors path="*" cssClass="error_message"/>
    <span>Title: </span><br><form:input pattern="[\D]{3,30}" type="text" path="title"/><br><br>
    <span>Brief: </span><br><form:textarea pattern="[\D]{3,100}" path="shortText" placeholder="Brief"/><br><br>
    <span>Full text: </span><br><form:textarea pattern="[\D]{3,2000}" path="fullText" placeholder="Full text"/><br><br>
    <form:input type="hidden" path="newsId" value="${NewsDTO.newsId}"/>

    <form:select multiple="multiple" path="authors">
        <form:options title="Select author(s):"/>
        <form:options items="${Authors}"  itemValue="authorId" itemLabel="authorName"/>
    </form:select>

   <form:select multiple="multiple" path="tags">
        <form:options title="Select tag(s):"/>
       <form:options items="${Tags}" itemValue="tagId" itemLabel="tagName"/>
    </form:select>

    <br><br>
    <form:button>Save</form:button>

</form:form>

