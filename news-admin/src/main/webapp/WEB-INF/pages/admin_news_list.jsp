<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="news_list">
    <br>
    <form:form method="post" commandName="SearchNewsDTO" action="search_news.do">
        <form:select path="author.authorId" itemLabel="authorName" itemValue="authorId"
                     items="${Authors}"/><br><br>
        <form:select path="tagsId" itemLabel="tagName" itemValue="tagId" items="${Tags}"
                     multiple="true"/>
        <br><br>
        <form:button>Search</form:button>
    </form:form>
    <br>
    <c:forEach items="${newsDTOs}" var="newsDTO" varStatus="loop">
        <form:form cssClass="inline_block_form" method="post" commandName="News" action="view_news_message.do">
            <form:input type="hidden" path="newsId" value="${newsDTO.newsId}"/>
            <form:button class="button_as_link">${newsDTO.title}</form:button>
        </form:form>

        <form:form cssClass="inline_block_form" commandName="News" method="post" action="edit_news_message.do">
            <form:input type="hidden" path="newsId" value="${newsDTO.newsId}"/>
            <input type="submit" class="edit_button" value=" "/>
        </form:form> <span> Comments(${newsDTO.comments.size()})</span><br>

        <span><i>Created:</i> ${newsDTO.creationDate}</span>
        <span><i>Last modified: </i>${newsDTO.modificationDate}</span><br>
        <p>${newsDTO.shortText}</p>

        <span class="dark_gray_text">Author(s): </span>
        <c:forEach items="${newsDTO.authors}" var="author" varStatus="status">
            <span class="dark_gray_text">${author.authorName}</span>
            <c:if test="${ not (status.last)}"><span class="dark_gray_text">,</span></c:if>
        </c:forEach><br>
        <span class="dark_gray_text">Tag(s): </span>
        <c:forEach items="${newsDTO.tags}" var="tag" varStatus="status">
            <span class="dark_gray_text">${tag.tagName}</span>
            <c:if test="${ not (status.last)}"><span class="dark_gray_text">,</span></c:if>
        </c:forEach><br><br>

    </c:forEach>

    <c:if test="${not empty pages_number}">
        <div class="pages">
            <c:forEach begin="1" end="${pages_number}" varStatus="iteration">
                <form class="inline_block_form">
                    <input type="hidden" value="${iteration.index}" name="page">
                    <button id="${iteration.index}" type="submit" href="/news_list.do">${iteration.index}</button>
                </form>
            </c:forEach>
        </div>
    </c:if>

</div>


