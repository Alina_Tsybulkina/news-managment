<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="./resources/admin_module.css">
</head>
<body>
<header>
    <tiles:insertAttribute name="helloAndLogout"/>
    <tiles:insertAttribute name="header"/>
</header>
<div class="main_div">
    <aside>
        <tiles:insertAttribute name="menu"/>
    </aside>
    <section>
        <tiles:insertAttribute name="main"/>
    </section>
</div>
<footer>
    <tiles:insertAttribute name="footer"/>
</footer>
</body>
</html>