<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<form:form action="update_news_message.do" method="post" modelAttribute="AddNewsDTO">
    <br>

    <div class="error_message">${error}</div>
    <form:input name="title" cssClass="title_input" pattern="[\D]{3,30}"  type="text" path="title" value="${NewsDTO.title}"/><br><br>
    <form:textarea name="shortText" path="shortText" placeholder="Brief"
                   value="${NewsDTO.shortText}"/><br><br>
    <form:textarea name="fullText" path="fullText" placeholder="Full text"
                   value="${NewsDTO.fullText}"/><br><br>


    <form:input type="hidden" path="newsId" value="${NewsDTO.newsId}"/>
    <form:input type="hidden" path="creationDate" value="${NewsDTO.creationDate}"/>

    <form:select multiple="multiple" path="authors">
        <form:options title="Select author(s):"/>
        <form:options items="${NewsDTO.authors}" itemValue="authorId" itemLabel="authorName"/>
    </form:select>

    <form:select multiple="multiple" path="tags">
        <form:options title="Select tag(s):"/>
        <form:options items="${NewsDTO.tags}" itemValue="tagId" itemLabel="tagName"/>
    </form:select> <br><br>

    <form:button onclick="return checkNews()">Save</form:button>
</form:form>

<form method="post" action="delete_news_message.do">
    <input type="hidden" name="newsId" value="${NewsDTO.newsId}">
    <input type="submit" value="Delete">
</form>

<script src="resources/scripts.js"></script>

