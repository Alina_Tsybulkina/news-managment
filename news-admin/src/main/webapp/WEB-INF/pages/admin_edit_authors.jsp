<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<br>
<c:forEach items="${Authors}" var="author" varStatus="loopStatus">
    <form:form cssClass="author_form" modelAttribute="Author" method="post">
        <c:if test="${author.authorId eq Author.authorId}">
            <form:errors cssClass="error_message" path="*"/>
        </c:if><br>
        <span>Author: </span>
        <form:input disabled="true" class="name_input" pattern="[\D]{2,30}" path="authorName" value="${author.authorName}"/>
        <form:input path="authorId" type="hidden" value="${author.authorId}"/>
        <input type="button" class="edit" onclick="show(this.parentNode)" value="Edit"/>
        <form:button class="edit_buttons" formAction="update_author.do">Update</form:button>
        <form:button class="edit_buttons" formAction="expire_author.do">Expire</form:button>
    </form:form>
</c:forEach>
<script src="resources/scripts.js"></script>

