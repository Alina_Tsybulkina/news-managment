<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="view_news">
    <h1>${NewsDTO.title}</h1>
    <span class="dark_gray_text">Author(s): </span>
    <c:forEach items="${NewsDTO.authors}" var="author" varStatus="status">
        <span class="dark_gray_text">${author.authorName}</span>
        <c:if test="${ not (status.last)}"><span class="dark_gray_text">,</span></c:if>
    </c:forEach><br>
    <span><i>Created:</i> ${NewsDTO.creationDate}</span>
    <span><i>Last modified:</i> ${NewsDTO.modificationDate}</span>
    <p>${NewsDTO.fullText}</p>
    <c:forEach items="${NewsDTO.comments}" var="comment">
        <span class="underline_text">${comment.creationDate}</span>
        <form:form commandName="Comment" cssClass="comment_area" action="delete_comment.do" method="post">
            <form:input type="hidden" path="commentId" value="${comment.commentId}"/>
            <form:input type="hidden" path="news.newsId" value="${NewsDTO.newsId}"/>
            <form:button class="right_float" type="submit">&#xD7;</form:button>
            <p>${comment.commentText}</p>
        </form:form>
    </c:forEach>

    <form:form commandName="Comment" method="post" action="add_comment.do">
        <form:textarea path="commentText" pattern="[\D]{3,100}" placeholder="Comment size should be within 3 and 100 symbols."/>
        <form:input type="hidden" path="news.newsId" value="${NewsDTO.newsId}"/><br>
        <form:button>Add comment</form:button>
    </form:form>

    <div class="navigation">
        <c:if test="${not (previous == NULL)}">
            <form:form cssClass="inline_block_form" method="post" commandName="News" action="view_news_message.do">
                <form:input type="hidden" path="newsId" value="${previous}"/>
                <form:button class="button_as_link">< Previous</form:button>
            </form:form>
        </c:if>

        <c:if test="${not (next == NULL)}">
            <form:form class="inline_block_form right_float" method="post" commandName="News"
                       action="view_news_message.do">
                <form:input type="hidden" path="newsId" value="${next}"/>
                <form:button class="button_as_link">Next ></form:button>
            </form:form>
        </c:if>
    </div>
</div>