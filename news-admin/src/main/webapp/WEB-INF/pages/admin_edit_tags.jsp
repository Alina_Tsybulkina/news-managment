<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<br>
<c:forEach items="${Tags}" var="tag" varStatus="loopStatus">

    <form:form modelAttribute="Tag" cssClass="tag_form" method="post">
        <span>Tag: </span>
        <form:input disabled="true" class="name_input" pattern="[\D]{2,30}" path="tagName" value="${tag.tagName}"/>
        <form:input path="tagId" type="hidden" value="${tag.tagId}"/>
        <input type="button" class="edit" onclick="show(this.parentNode)" value="Edit"/>
        <form:button class="edit_buttons" formAction="update_tag.do">Update</form:button>
        <form:button class="edit_buttons" formAction="delete_tag.do">Delete</form:button>
    </form:form>
</c:forEach>

<script src="resources/scripts.js"></script>
