<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div>
    <form class="login_form" method="post" action="j_spring_security_check">
        <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
            <p class="error_message">
                Your login attempt was not successful due to <br/><br/>
                <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}."/>
            </p>
        </c:if>
        <br><br>
        <input type="text" name="login" placeholder="Login"/><br><br>
        <input type="password" name="password" placeholder="Password"/><br><br>
        <button type="submit">Log in</button>
    </form>
</div>
