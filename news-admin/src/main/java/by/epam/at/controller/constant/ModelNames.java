package by.epam.at.controller.constant;

/**
 * Names of models which send to jsp pages.
 */
public final class ModelNames {
    public static final String AUTHOR = "Author";
    public static final String AUTHORS = "Authors";
    public static final String COMMENT = "Comment";
    public static final String NEWS = "News";
    public static final String NEWS_DTOS = "newsDTOs";
    public static final String ADD_NEWS_DTO = "AddNewsDTO";
    public static final String NEWS_DTO = "NewsDTO";
    public static final String TAGS = "Tags";
    public static final String USER = "User";
    public static final String USER_DTO = "UserDTO";
    public static final String TAG = "Tag";
    public static final String SEARCH_NEWS_DTO = "SearchNewsDTO";
    public static final String PAGED_LIST_HOLDER = "pagedListHolder";
    public static final String PREVIOUS = "previous";
    public static final String NEXT = "next";
    public static final String ERROR = "error";
    public static final String CURRENT_PAGE = "current_page";
    public static final String PAGES_NUMBER = "pages_number";
}
