package by.epam.at.controller;

import by.epam.at.controller.constant.JspPages;
import by.epam.at.controller.constant.ModelNames;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @RequestMapping(value = "/login_admin_page", method = RequestMethod.GET)
    public ModelAndView getAdminLoginPage(@RequestParam(value = "error", required = false) String error) {
        ModelAndView modelAndView = new ModelAndView(JspPages.NEWS_ADMIN_LOGIN.name().toLowerCase());
        if (error != null) {
            modelAndView.addObject(ModelNames.ERROR, error);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/error")
    public ModelAndView getErrorPage() {
        ModelAndView modelAndView = new ModelAndView(JspPages.ERROR.name().toLowerCase());
        return modelAndView;
    }

}
