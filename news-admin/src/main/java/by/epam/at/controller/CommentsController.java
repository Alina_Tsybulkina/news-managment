package by.epam.at.controller;

import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import by.epam.alina.service.CommentService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.at.controller.constant.ModelNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;

@Controller
public class CommentsController {

    private static final Logger LOGGER = LogManager.getLogger(CommentsController.class);

    @Autowired
    private CommentService commentService;

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/delete_comment", method = RequestMethod.POST)
    public ModelAndView deleteComment(@ModelAttribute(ModelNames.COMMENT) Comment comment, RedirectAttributes redirectAttributes){
        try {
            commentService.deleteComment(comment.getCommentId());
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        News news = new News();
        news.setNewsId(comment.getNews().getNewsId());
        redirectAttributes.addFlashAttribute(ModelNames.NEWS, news);
        return new ModelAndView("redirect:/view_news_message.do");
    }

    @RequestMapping(value = "/add_comment", method = RequestMethod.POST)
    public ModelAndView addComment(@ModelAttribute(ModelNames.COMMENT) Comment comment, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        News news = new News();
        news.setNewsId(comment.getNews().getNewsId());
        try {
            if (!bindingResult.hasErrors()) {
                comment.setCreationDate(new Timestamp(new Date().getTime()));
                commentService.addComment(comment);
            } 
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        redirectAttributes.addFlashAttribute(ModelNames.NEWS, news);
        return new ModelAndView("redirect:/view_news_message.do");
    }
}
