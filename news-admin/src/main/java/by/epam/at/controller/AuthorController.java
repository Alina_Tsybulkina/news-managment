package by.epam.at.controller;

import by.epam.alina.entity.Author;
import by.epam.alina.service.AuthorService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.at.controller.constant.JspPages;
import by.epam.at.controller.constant.ModelNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;
import java.util.Set;

@Controller
public class AuthorController {

    private static final Logger LOGGER = LogManager.getLogger(AuthorController.class);

    @Autowired
    private AuthorService authorService;

    @Autowired
    private Validator validator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(this.validator);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/update_authors_page")
    public ModelAndView getAuthorsPage() {
        Set<Author> authors = null;
        try {
            authors = authorService.getAllAvailable();
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_EDIT_AUTHORS.name().toLowerCase(), ModelNames.AUTHORS, authors);
        modelAndView.addObject(ModelNames.AUTHOR, new Author());
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/update_author", method = RequestMethod.POST)
    public ModelAndView updateAuthor(@ModelAttribute(ModelNames.AUTHOR) @Valid Author author, BindingResult bindingResult) {
        Set<Author> authors = null;
        try {
            if (!bindingResult.hasErrors()) {
                authorService.update(author);
            }
            authors = authorService.getAllAvailable();
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_EDIT_AUTHORS.name().toLowerCase(), ModelNames.AUTHORS, authors);
        modelAndView.addObject(ModelNames.AUTHOR, new Author());
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/expire_author", method = RequestMethod.POST)
    public ModelAndView expireAuthor(@ModelAttribute(ModelNames.AUTHOR) Author author) {
        try {
            authorService.expire(author);
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/update_authors_page.do");
    }
}
