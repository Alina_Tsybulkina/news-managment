package by.epam.at.controller.constant;

public enum JspPages {

    NEWS_ADMIN_LOGIN, ADMIN_NEWS_LIST,
    ADMIN_EDIT_NEWS_MESSAGE, ADMIN_ADD_NEWS,
    ADMIN_EDIT_AUTHORS, ADMIN_EDIT_TAGS,
    ADMIN_VIEW_NEWS, ERROR
}
