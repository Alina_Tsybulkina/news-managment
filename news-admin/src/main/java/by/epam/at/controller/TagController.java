package by.epam.at.controller;

import by.epam.alina.entity.Tag;
import by.epam.alina.service.TagService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.at.controller.constant.JspPages;
import by.epam.at.controller.constant.ModelNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    public static final Logger LOGGER = LogManager.getLogger(TagController.class);

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/update_tags_page", method = RequestMethod.GET)
    public ModelAndView getTagsPage(){
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_EDIT_TAGS.name().toLowerCase());
        try {
            modelAndView.addObject(ModelNames.TAGS, tagService.getAll());
            modelAndView.addObject(ModelNames.TAG, new Tag());
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/update_tag", method = RequestMethod.POST)
    public ModelAndView updateTag(@ModelAttribute(ModelNames.TAG)@Valid Tag tag, BindingResult bindingResult){
        try {
            if (! bindingResult.hasErrors()) {
                tagService.update(tag);
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/update_tags_page.do");
    }


    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/delete_tag", method = RequestMethod.POST)
    public ModelAndView deleteTag(@ModelAttribute(ModelNames.TAG) Tag tag){
        try{
            tagService.delete(tag.getTagId());
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/update_tags_page.do");
    }
}
