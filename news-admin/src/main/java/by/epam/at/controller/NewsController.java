package by.epam.at.controller;

import by.epam.alina.entity.Author;
import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import by.epam.alina.entity.Tag;
import by.epam.alina.entity.dto.SearchNewsDTO;
import by.epam.alina.service.AuthorService;
import by.epam.alina.service.NewsService;
import by.epam.alina.service.TagService;
import by.epam.alina.service.exception.ServiceException;
import by.epam.at.controller.constant.JspPages;
import by.epam.at.controller.constant.ModelNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@Controller
public class NewsController {

    private static final Logger LOGGER = LogManager.getLogger(NewsController.class);

    @Autowired
    private NewsService newsService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/news_list", method = RequestMethod.GET)
    public ModelAndView getNewsList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_NEWS_LIST.name().toLowerCase());
        List<News> newsSet = null;
        Set<Author> authors = null;
        Set<Tag> tags = null;
        try {
            newsSet = newsService.getSortedNews(page);
            authors = authorService.getAll();
            tags = tagService.getAll();
            long numberOfNews = newsService.countNews();
            modelAndView.addObject(ModelNames.PAGES_NUMBER, numberOfNews % 3 == 0? numberOfNews/3 : numberOfNews/3 + 1);

        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        modelAndView.addObject(ModelNames.CURRENT_PAGE, page);
        modelAndView.addObject(ModelNames.NEWS_DTOS, newsSet);
        modelAndView.addObject(ModelNames.NEWS, new News());
        modelAndView.addObject(ModelNames.SEARCH_NEWS_DTO, new SearchNewsDTO());
        modelAndView.addObject(ModelNames.AUTHORS, authors);
        modelAndView.addObject(ModelNames.TAGS, tags);
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/search_news", method = RequestMethod.POST)
    public ModelAndView searchNews(@ModelAttribute(ModelNames.SEARCH_NEWS_DTO) @Valid SearchNewsDTO searchNewsDTO, BindingResult bindingResult) {
        Set<News> newsSet = null;
        Set<Author> authors = null;
        Set<Tag> tags = null;
        try {
            if (! bindingResult.hasErrors()) {
                newsSet = newsService.searchNews(searchNewsDTO);
            }
            authors = authorService.getAll();
            tags = tagService.getAll();
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_NEWS_LIST.name().toLowerCase());
        modelAndView.addObject(ModelNames.NEWS_DTOS, newsSet);
        modelAndView.addObject(ModelNames.NEWS, new News());
        modelAndView.addObject(ModelNames.SEARCH_NEWS_DTO, new SearchNewsDTO());
        modelAndView.addObject(ModelNames.AUTHORS, authors);
        modelAndView.addObject(ModelNames.TAGS, tags);
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/update_news_message", method = RequestMethod.POST)
    public ModelAndView updateNews(@ModelAttribute(ModelNames.ADD_NEWS_DTO) @Valid News news, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        try{
            if (! bindingResult.hasErrors()) {
                news.setModificationDate(new Date(Calendar.getInstance().getTime().getTime()));
                newsService.updateNews(news);
            } else {
                redirectAttributes.addFlashAttribute(ModelNames.NEWS, news);
                return new ModelAndView("redirect:/edit_news_message.do");
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/news_list.do");
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/edit_news_message", method = {RequestMethod.POST, RequestMethod.GET
    })
    public ModelAndView editNews(@ModelAttribute(ModelNames.NEWS) News news){
        try {
            news = newsService.getOne(news.getNewsId());
            news.setTags(tagService.getAll());
            news.setAuthors(authorService.getAllAvailable());
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_EDIT_NEWS_MESSAGE.name().toLowerCase());
        modelAndView.addObject(ModelNames.ADD_NEWS_DTO, new News());
        modelAndView.addObject(ModelNames.NEWS_DTO, news);
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/view_news_message", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView viewNewsMessage(@ModelAttribute(ModelNames.NEWS) News news){
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_VIEW_NEWS.name().toLowerCase());
        try {
            news = newsService.getOne(news.getNewsId());
            modelAndView.addObject(ModelNames.PREVIOUS, newsService.getPrevious(news.getNewsId()));
            modelAndView.addObject(ModelNames.NEXT, newsService.getNext(news.getNewsId()));
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        modelAndView.addObject(ModelNames.NEWS_DTO, news);
        modelAndView.addObject(ModelNames.COMMENT, new Comment());
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/add_news_page", method = RequestMethod.GET)
    public ModelAndView addNewsPage(){
        Set<Tag> tags = null;
        Set<Author> authors = null;
        News news = new News();
        ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_ADD_NEWS.name().toLowerCase());
        try {
            tags = tagService.getAll();
            authors = authorService.getAllAvailable();
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        modelAndView.addObject(ModelNames.ADD_NEWS_DTO, news);
        modelAndView.addObject(ModelNames.TAGS, tags);
        modelAndView.addObject(ModelNames.AUTHORS, authors);
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/add_news_message", method = RequestMethod.POST)
    public ModelAndView addNewsMessage(@ModelAttribute(ModelNames.ADD_NEWS_DTO) @Valid News news, BindingResult bindingResult){
        try {
            if (! bindingResult.hasErrors()) {
                news.setCreationDate(new Timestamp(new java.util.Date().getTime()));
                news.setModificationDate(new Date(Calendar.getInstance().getTime().getTime()));
                newsService.insertNews(news);
            } else {
                ModelAndView modelAndView = new ModelAndView(JspPages.ADMIN_ADD_NEWS.name().toLowerCase());
                modelAndView.addObject(ModelNames.TAGS, tagService.getAll());
                modelAndView.addObject(ModelNames.AUTHORS, authorService.getAll());
                return modelAndView;
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/news_list.do");
    }

    @RequestMapping(value = "/delete_news_message", method = RequestMethod.POST)
    public ModelAndView deleteNewsMessage(@RequestParam("newsId") long newsId){
        try {
            newsService.deleteNews(newsId);
        }catch (ServiceException e) {
            LOGGER.error(e);
        }
        return new ModelAndView("redirect:/news_list.do");
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.registerCustomEditor(Set.class, "tags", new CustomCollectionEditor(Set.class) {
            protected Object convertElement(Object element) {
                Tag tag = new Tag();
                tag.setTagId(Long.parseLong(((String)element)));
                return tag;
            }
        });

        binder.registerCustomEditor(Set.class, "authors", new CustomCollectionEditor(Set.class) {
            protected Object convertElement(Object element) {
                Author author = new Author();
                author.setAuthorId(Long.valueOf(((String)element)));
                return author;
            }
        });
    }
}
