package by.epam.alina.service.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.dao.NewsDAO;
import by.epam.alina.dao.TagDAO;
import by.epam.alina.entity.Author;
import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import by.epam.alina.entity.Tag;
import by.epam.alina.entity.dto.SearchNewsDTO;
import by.epam.alina.service.AuthorService;
import by.epam.alina.service.TagService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import java.sql.Timestamp;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

    @InjectMocks
    private NewsServiceImpl newsService;

    @Mock
    private NewsDAO newsDAO;

    @Mock
    private TagDAO tagDAO;

    @Mock
    private AuthorDAO authorDAO;

    @Mock
    private CommentsDAO commentsDAO;

    @Mock
    private TagService tagService;

    @Mock
    private AuthorService authorService;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsertNews() throws Exception {
        News news = new News();
        news.setNewsId(1L);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(new Date().getTime()));
        news.setModificationDate(new java.sql.Date(new Date().getTime()));
        Set<Long> authors = new HashSet<>(1);
        authors.add(3L);
        Set<Long> tags = new HashSet<>(1);
        tags.add(3L);
        newsService.insertNews(news);
        when(newsDAO.getById(1L)).thenReturn(news);
        Assert.assertEquals(newsService.getOne(1L), news);
    }

    @Test
    public void testUpdateNews() throws Exception {
        News news = new News();
        news.setNewsId(1L);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(new Date().getTime()));
        news.setModificationDate(new java.sql.Date(new Date().getTime()));
        newsService.updateNews(news);
        when(newsDAO.getById(1L)).thenReturn(news);
        Assert.assertEquals(news, newsDAO.getById(1L));
    }

    @Test
    public void testDeleteNews() throws Exception {
        when(newsDAO.getById(1L)).thenReturn(null);
        newsService.deleteNews(1L);
        Assert.assertEquals(null, newsDAO.getById(1L));
    }

    @Test
    public void testGetAllNews() throws Exception {
        News news = new News();
        news.setNewsId(1L);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(new Date().getTime()));
        news.setModificationDate(new java.sql.Date(new Date().getTime()));
        List<News> newsSet = new ArrayList<>(1);
        newsSet.add(news);
        when(newsDAO.getAll()).thenReturn(newsSet);
        assertThat(newsService.getAllNews()).contains(news);
    }

    @Test
    public void testGetOne() throws Exception {
        News news = new News();
        news.setNewsId(1L);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(new Date().getTime()));
        news.setModificationDate(new java.sql.Date(new Date().getTime()));
        when(newsDAO.getById(1L)).thenReturn(news);
        Assert.assertEquals(news, newsDAO.getById(1L));
    }

    @Test
    public void testCountNews() throws Exception {
        when(newsDAO.getNumberOfNews()).thenReturn(1L);
        Assert.assertEquals(newsService.countNews(), 1);
    }

    @Test
    public void testSearchNews() throws Exception {
        SearchNewsDTO dto = new SearchNewsDTO();
        Set<Long> tags = new HashSet<>(1);
        tags.add(1l);
        dto.setTagsId(tags);
        Author author = new Author();
        author.setAuthorId(1L);
        author.setAuthorName("Ivan Ivanov");
        dto.setAuthor(author);
        News news = new News();
        news.setNewsId(1l);
        news.setTitle("Test");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(Timestamp.valueOf("1996-06-20 20:15:11"));
        news.setModificationDate(new java.sql.Date(Timestamp.valueOf("1996-06-20 20:15:11").getTime()));
        Set<News> newsSet = new HashSet<News>(1);
        newsSet.add(news);
        when(newsDAO.searchNews(dto)).thenReturn(newsSet);

        Assert.assertEquals(newsSet, newsService.searchNews(dto));
    }
}