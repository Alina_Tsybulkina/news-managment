package by.epam.alina.service.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.entity.Author;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private AuthorDAO authorDAO;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsert() throws Exception {
        Author author = new Author();
        author.setAuthorId(1L);
        when(authorDAO.insert(author)).thenReturn(1L);
        authorService.insert(author);
        Assert.assertEquals(new Long(1L), author.getAuthorId());
    }

    @Test
    public void testGetById() throws Exception {
        Author author = new Author();
        author.setAuthorId(1L);
        when(authorDAO.getById(1L)).thenReturn(author);
        Author newAuthor = authorService.getById(1L);
        Assert.assertEquals(author, newAuthor);
    }

    @Test
    public void testGetByName() throws Exception {
        Author author = new Author();
        author.setAuthorName("Alina");
        when(authorDAO.getByName(author)).thenReturn(author);
        Author newAuthor = authorService.getByName(author);
        Assert.assertEquals(author, newAuthor);
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Set<Author> authors = new HashSet<Author>();
        Author author = new Author();
        author.setAuthorId(0L);
        authors.add(author);
        when(authorDAO.getByNewsId(1L)).thenReturn(authors);
        Set<Author> newAuthors = authorDAO.getByNewsId(1L);
        Assert.assertEquals(authors, newAuthors);
    }

    @Test
    public void testGetAll() throws Exception {
        Set<Author> authors = new HashSet<Author>();
        Author author = new Author();
        author.setAuthorId(0L);
        authors.add(author);
        when(authorDAO.getAll()).thenReturn(authors);
        Set<Author> newAuthors = authorDAO.getAll();
        Assert.assertEquals(authors, newAuthors);
    }
}