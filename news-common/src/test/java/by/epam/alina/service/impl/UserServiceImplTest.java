package by.epam.alina.service.impl;

import by.epam.alina.dao.UserDAO;
import by.epam.alina.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import java.util.HashSet;
import java.util.Set;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDAO userDAO;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsert() throws Exception {
        User user = new User();
        user.setUserId(1L);
        user.setLogin("test");
        user.setUserName("test");
        user.setPassword("test");
        userService.insert(user);
        when(userDAO.getById(1L)).thenReturn(user);
        Assert.assertEquals(user, userService.getById(1L));
    }

    @Test
    public void testUpdate() throws Exception {
        User user = new User();
        user.setUserId(1L);
        user.setLogin("test");
        user.setUserName("test");
        user.setPassword("test");
        userService.update(user);
        when(userDAO.getById(1L)).thenReturn(user);
        Assert.assertEquals(user, userService.getById(1L));
    }

    @Test
    public void testDelete() throws Exception {
        userService.delete(1l);
        when(userDAO.getById(1L)).thenReturn(null);
        Assert.assertEquals(userService.getById(1L), null);
    }

    @Test
    public void testGetById() throws Exception {
        User user = new User();
        user.setUserId(1L);
        user.setLogin("test");
        user.setUserName("test");
        user.setPassword("test");
        when(userDAO.getById(1l)).thenReturn(user);
        Assert.assertEquals(user, userService.getById(1L));
    }

    @Test
    public void testGetAll() throws Exception {
        Set<User> users = new HashSet<User>(1);
        User user = new User();
        user.setUserId(1L);
        user.setLogin("test");
        user.setUserName("test");
        user.setPassword("test");
        users.add(user);
        when(userDAO.getAll()).thenReturn(users);
        assertThat(userDAO.getAll()).contains(user);
    }
}