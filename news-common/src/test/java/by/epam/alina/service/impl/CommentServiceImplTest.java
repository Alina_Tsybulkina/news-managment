package by.epam.alina.service.impl;

import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.entity.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

    @InjectMocks
    private CommentServiceImpl commentService;

    @Mock
    private CommentsDAO commentsDAO;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddComment() throws Exception {
        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.getNews().setNewsId(1L);
        comment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        comment.setCommentText("Something");
        when(commentsDAO.insert(comment)).thenReturn(1L);
        commentService.addComment(comment);
        Assert.assertEquals(new Long(1L), comment.getCommentId());
    }


    @Test
    public void testGetById() throws Exception {
        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.getNews().setNewsId(1L);
        comment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        comment.setCommentText("Something");
        when(commentsDAO.getById(1L)).thenReturn(comment);
        Assert.assertEquals(comment, commentService.getById(1L));
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Set<Comment> comments = new HashSet<Comment>(1);
        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.getNews().setNewsId(1L);
        comment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        comment.setCommentText("Something");
        comments.add(comment);
        when(commentsDAO.getByNewsId(1L)).thenReturn(comments);
        Assert.assertEquals(comments, commentService.getByNewsId(1L));
    }

    @Test
    public void testGetAll() throws Exception {
        Set<Comment> comments = new HashSet<Comment>(1);
        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.getNews().setNewsId(1L);
        comment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        comment.setCommentText("Something");
        comments.add(comment);
        when(commentsDAO.getAll()).thenReturn(comments);
        Assert.assertEquals(comments, commentService.getAll());
    }
}