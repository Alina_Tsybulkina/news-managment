package by.epam.alina.service.impl;

import by.epam.alina.dao.TagDAO;
import by.epam.alina.entity.Tag;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.HashSet;
import java.util.Set;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

    @InjectMocks
    private TagServiceImpl tagService;

    @Mock
    private TagDAO tagDAO;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsert() throws Exception {
        Tag tag = new Tag();
        tag.setTagName("Something");
        tagService.insert(tag);
        when(tagDAO.getById(1L)).thenReturn(tag);
        Assert.assertEquals(tag, tagService.getById(1L));
    }

    @Test
    public void testUpdate() throws Exception {
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setTagName("Something");
        tagService.insert(tag);
        when(tagDAO.getById(1L)).thenReturn(tag);
        Assert.assertEquals(tag, tagService.getById(1L));
    }

    @Test
    public void testDelete() throws Exception {
        tagService.delete(1L);
        when(tagDAO.getById(1L)).thenReturn(null);
        Assert.assertEquals(null, tagService.getById(1L));
    }

    @Test
    public void testGetById() throws Exception {
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setTagName("Something");
        when(tagDAO.getById(1L)).thenReturn(tag);
        Assert.assertEquals(tag, tagService.getById(1L));
    }

    @Test
    public void testGetByName() throws Exception {
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setTagName("Something");
        when(tagDAO.getByName(tag)).thenReturn(tag);
        Assert.assertEquals(tag, tagService.getByName(tag));
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setTagName("Religion");
        Set<Tag> tagSet = new HashSet<Tag>(1);
        tagSet.add(tag);
        when(tagDAO.getByNewsId(1L)).thenReturn(tagSet);
        assertThat(tagService.getByNewsId(1L)).contains(tag);
    }
}