package by.epam.alina.service.impl;

import by.epam.alina.dao.RoleDAO;
import by.epam.alina.entity.Role;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTest {

    @InjectMocks
    private RoleServiceImpl roleService;

    @Mock
    private RoleDAO roleDAO;

    @Test
    public void testInsert() throws Exception {
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        roles.add(role);

        roleService.insert(role);
        when(roleDAO.getByUserId(1L)).thenReturn(roles);
        assertThat(roleService.getByUserId(1L)).contains(role);
    }

    @Test
    public void testUpdate() throws Exception {
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        roles.add(role);
        role.getUser().setUserId(1L);
        roleService.update(role);
        when(roleDAO.getByUserId(1L)).thenReturn(roles);
        assertThat(roleService.getByUserId(1L)).contains(role);
    }

    @Test
    public void testDelete() throws Exception {
        roleService.delete(1L);
        when(roleDAO.getByUserId(1L)).thenReturn(null);
        Assert.assertEquals(null, roleService.getByUserId(1L));
    }

    @Test
    public void testGetByUserId() throws Exception {
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        roles.add(role);
        role.getUser().setUserId(1L);
        when(roleDAO.getByUserId(1L)).thenReturn(roles);
        assertThat(roleService.getByUserId(1L)).contains(role);
    }
}