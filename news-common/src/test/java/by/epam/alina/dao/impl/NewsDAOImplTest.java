package by.epam.alina.dao.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.dao.NewsDAO;
import by.epam.alina.dao.TagDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Author;
import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import by.epam.alina.entity.Tag;
import by.epam.alina.entity.dto.SearchNewsDTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class NewsDAOImplTest {

    private final News firstNewsObject, secondNewsObject;
    private final Author firstAuthor, secondAuthor;
    private final Comment firstComment, secondComment;

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private CommentsDAO commentsDAO;

    {
        firstNewsObject = new News();
        firstNewsObject.setNewsId(1L);
        firstNewsObject.setTitle("Test");
        firstNewsObject.setShortText("Short text");
        firstNewsObject.setFullText("Full text");
        firstNewsObject.setCreationDate(Timestamp.valueOf("1996-06-20 20:15:11"));
        firstNewsObject.setModificationDate(new java.sql.Date(Timestamp.valueOf("1996-06-20 20:15:11").getTime()));

        secondNewsObject = new News();
        secondNewsObject.setNewsId(2L);
        secondNewsObject.setTitle("Test2");
        secondNewsObject.setShortText("Short text");
        secondNewsObject.setFullText("Full text");
        secondNewsObject.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        secondNewsObject.setModificationDate(new java.sql.Date(Timestamp.valueOf("1999-06-20 20:15:11").getTime()));

        firstAuthor = new Author();
        firstAuthor.setAuthorId(1L);
        firstAuthor.setAuthorName("Ivan Ivanov");

        secondAuthor = new Author();
        secondAuthor.setAuthorId(3L);
        secondAuthor.setAuthorName("Kto-to");

        firstComment = new Comment();
        firstComment.setCommentId(1L);
        firstComment.setCommentText("Something");
        firstComment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        firstComment.getNews().setNewsId(1L);

        secondComment = new Comment();
        secondComment.setCommentId(3L);
        secondComment.setCommentText("Hello");
        secondComment.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        secondComment.getNews().setNewsId(1L);
    }

    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        News news = new News();
        news.setNewsId(0L);
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(new Date().getTime()));
        news.setModificationDate(new java.sql.Date(new Date().getTime()));
        news.getAuthors().add(secondAuthor);
        newsDAO.insert(news);
        Assert.assertNotEquals(news.getNewsId(), new Long(0L));
        assertThat(authorDAO.getByNewsId(1L)).contains(secondAuthor);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        firstNewsObject.setModificationDate(new java.sql.Date(new Date().getTime()));
        firstNewsObject.getAuthors().add(secondAuthor);

        newsDAO.update(firstNewsObject);

        Assert.assertEquals(newsDAO.getById(1L), firstNewsObject);
        Assert.assertEquals(authorDAO.getByNewsId(1L).contains(secondAuthor), true);
        Assert.assertEquals(authorDAO.getByNewsId(1L).contains(firstAuthor), false);
        assertThat(commentsDAO.getByNewsId(1L)).contains(firstComment, secondComment);
        assertThat(tagDAO.getByNewsId(1L)).isEmpty();

        firstNewsObject.getAuthors().remove(secondAuthor);
        firstNewsObject.setModificationDate(new java.sql.Date(Timestamp.valueOf("1996-06-20 20:15:11").getTime()));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        newsDAO.delete(1L);
        Assert.assertEquals(newsDAO.getById(1L), null);
        Assert.assertEquals(authorDAO.getById(1L), firstAuthor);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetById() throws Exception {
        Assert.assertEquals(newsDAO.getById(1L), firstNewsObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetNumberOfNews() throws Exception {
        long number = newsDAO.getNumberOfNews();
        Assert.assertEquals(number, newsDAO.getAll().size());
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetAll() throws Exception {
        assertThat(newsDAO.getAll()).contains(firstNewsObject, secondNewsObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetMostCommented() throws Exception {
        List<News> newsList = newsDAO.getMostCommented(1);
        assertThat(newsList).isNotEmpty();
        assertThat(newsList.size()).isEqualTo(2);
        if (newsList.size() > 1) {
            for (int i = 1; i < newsList.size(); i++) {
                assertThat(newsList.get(i - 1).getComments().size())
                        .isGreaterThanOrEqualTo(newsList.get(i).getComments().size());
            }
        }
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetNext() throws DaoException {
        Assert.assertEquals(null, newsDAO.getNext(2L));
        Assert.assertEquals(new Long(2L), newsDAO.getNext(1L));

    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetPrevious() throws DaoException
    {
        Assert.assertEquals(new Long(1L), newsDAO.getPrevious(2L));
        Assert.assertEquals(null, newsDAO.getPrevious(1L));

    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testSearchNews() throws Exception {
        SearchNewsDTO dto = new SearchNewsDTO();
        Set<Long> tags = new HashSet<>(1);
        tags.add(1L);
        dto.setTagsId(tags);
        Author author = new Author();
        author.setAuthorId(1L);
        author.setAuthorName("Ivan Ivanov");
        dto.setAuthor(author);
        assertThat(newsDAO.searchNews(dto)).containsOnly(firstNewsObject);
    }
}