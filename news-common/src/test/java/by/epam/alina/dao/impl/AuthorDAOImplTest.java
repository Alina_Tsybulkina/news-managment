package by.epam.alina.dao.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class,
    TransactionalTestExecutionListener.class})
public class AuthorDAOImplTest {

    private final Author firstAuthorObject, secondAuthorObject;

    {
        firstAuthorObject = new Author();
        secondAuthorObject = new Author();
        firstAuthorObject.setAuthorId(1L);
        firstAuthorObject.setAuthorName("Ivan Ivanov");
        secondAuthorObject.setAuthorId(2L);
        secondAuthorObject.setAuthorName("Pavel Pavlov");
    }

    @Autowired
    private AuthorDAO authorDAO;
    
    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        Author author = new Author();
        author.setAuthorId(0L);
        author.setAuthorName("Test");
        Long id = authorDAO.insert(author);
        Assert.assertNotEquals(new Long(0L), id);
        author.setAuthorId(id);
        Assert.assertNotEquals(authorDAO.getById(id), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        firstAuthorObject.setAuthorName("Test");
        authorDAO.update(firstAuthorObject);
        Author author = authorDAO.getById(1L);
        Assert.assertEquals(author, firstAuthorObject);
        firstAuthorObject.setAuthorName("Ivan Ivanov");
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        authorDAO.delete(1L);
        Assert.assertEquals(authorDAO.getById(1L), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetById() throws Exception {
        Author author = authorDAO.getById(1L);
        Assert.assertEquals(author, firstAuthorObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByName() throws Exception {
        Author author = authorDAO.getByName(firstAuthorObject);
        Assert.assertEquals(firstAuthorObject, author);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByNewsId() throws Exception {
        Set<Author> authors = new HashSet<>();
        authors.add(firstAuthorObject);
        authors.add(secondAuthorObject);
        Assert.assertEquals(authors, authorDAO.getByNewsId(1L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetAll() throws Exception {
        Set<Author> searchResults = authorDAO.getAll();
        assertThat(searchResults.size()).isEqualTo(3);
    }

}
