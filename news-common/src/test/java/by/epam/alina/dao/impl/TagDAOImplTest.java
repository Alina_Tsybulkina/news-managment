package by.epam.alina.dao.impl;

import by.epam.alina.dao.TagDAO;
import by.epam.alina.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class TagDAOImplTest {

    private Tag firstTagObject, secondTagObject;


    @Autowired
    private TagDAO tagDAO;

    {
        firstTagObject = new Tag();
        firstTagObject.setTagId(1L);
        firstTagObject.setTagName("Religion");
        secondTagObject = new Tag();
        secondTagObject.setTagId(2L);
        secondTagObject.setTagName("Belarus");
    }

    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        Tag tag = new Tag();
        tag.setTagName("something");
        tagDAO.insert(tag);
        Assert.assertNotEquals(tag.getTagId(), new Long(0L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        firstTagObject.setTagName("something");
        tagDAO.update(firstTagObject);
        Assert.assertEquals(tagDAO.getById(1L), firstTagObject);
        firstTagObject.setTagName("Religion");
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        tagDAO.delete(1L);
        Assert.assertEquals(tagDAO.getById(1L), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetById() throws Exception {
        Assert.assertEquals(tagDAO.getById(1L), firstTagObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByName() throws Exception {
        Assert.assertEquals(tagDAO.getByName(firstTagObject), firstTagObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByNewsId() throws Exception {
        assertThat(tagDAO.getByNewsId(1L)).contains(firstTagObject, secondTagObject);
    }
}