package by.epam.alina.dao.impl;

import by.epam.alina.dao.UserDAO;
import by.epam.alina.entity.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class UserDAOImplTest {

    private User firstUserObject, secondUserObject;

    @Autowired
    private UserDAO userDAO;

    {
        firstUserObject = new User();
        firstUserObject.setUserId(1L);
        firstUserObject.setLogin("alina");
        firstUserObject.setUserName("Alina");
        firstUserObject.setPassword("alina");
        secondUserObject = new User();
        secondUserObject.setUserId(2L);
        secondUserObject.setUserName("Test");
        secondUserObject.setLogin("test");
        secondUserObject.setPassword("test");
    }

    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        User user = new User();
        user.setLogin("test");
        user.setUserName("test");
        user.setPassword("test");
        userDAO.insert(user);
        Assert.assertNotEquals(user.getUserId(), new Long(0L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        firstUserObject.setLogin("new");
        userDAO.update(firstUserObject);
        Assert.assertEquals(userDAO.getById(1L), firstUserObject);
        firstUserObject.setLogin("alina");
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        userDAO.delete(1L);
        Assert.assertEquals(userDAO.getById(1L), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetById() throws Exception {
        Assert.assertEquals(userDAO.getById(1L), firstUserObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetAll() throws Exception {
        assertThat(userDAO.getAll()).contains(firstUserObject, secondUserObject);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByLogin() throws Exception
    {
        Assert.assertEquals(userDAO.getByLogin(firstUserObject), userDAO.getById(firstUserObject.getUserId()));
    }
}