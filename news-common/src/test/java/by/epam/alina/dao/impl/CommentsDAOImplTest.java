package by.epam.alina.dao.impl;

import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import static org.assertj.core.api.Assertions.assertThat;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class CommentsDAOImplTest {
    private Comment firstCommentObject, secondCommentObject;
    
    {
        firstCommentObject = new Comment();
        firstCommentObject.setCommentId(1L);
        firstCommentObject.setNews(new News());
        firstCommentObject.getNews().setNewsId(1L);
        firstCommentObject.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        firstCommentObject.setCommentText("Something");
        secondCommentObject = new Comment();
        secondCommentObject.setCommentId(3L);
        secondCommentObject.setNews(new News());
        secondCommentObject.getNews().setNewsId(1L);
        secondCommentObject.setCreationDate(Timestamp.valueOf("2016-06-20 20:15:11"));
        secondCommentObject.setCommentText("Hello");
    }

    @Autowired
    private CommentsDAO commentsDAO;

    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        Comment comment = new Comment();
        comment.getNews().setNewsId(1L);
        comment.setCreationDate(new Timestamp(new Date().getTime()));
        comment.setCommentText("Something");
        commentsDAO.insert(comment);
        Assert.assertNotEquals(comment.getCommentId(), new Long(0L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        firstCommentObject.setCommentText("Updated");
        commentsDAO.update(firstCommentObject);
        Assert.assertEquals(firstCommentObject, commentsDAO.getById(1L));
        firstCommentObject.setCommentText("Something");
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        commentsDAO.delete(1L);
        Assert.assertEquals(commentsDAO.getById(1L), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetById() throws Exception {
        Assert.assertEquals(firstCommentObject, commentsDAO.getById(1L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByNewsId() throws Exception {
        Set<Comment> comments = new HashSet<>(2);
        comments.add(firstCommentObject);
        comments.add(secondCommentObject);
        Assert.assertEquals(comments, commentsDAO.getByNewsId(1L));
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetAll() throws Exception {
        assertThat(commentsDAO.getAll()).contains(firstCommentObject, secondCommentObject);
    }
}