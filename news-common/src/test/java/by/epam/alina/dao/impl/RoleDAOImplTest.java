package by.epam.alina.dao.impl;

import by.epam.alina.dao.RoleDAO;
import by.epam.alina.entity.Role;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-test.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class RoleDAOImplTest {

    private Role roleObject;

    @Autowired
    private RoleDAO roleDAO;

    {
        roleObject = new Role();
        roleObject.setRoleId(1L);
        roleObject.getUser().setUserId(1L);
        roleObject.setRoleName("admin");
    }

    @Transactional
    @Rollback
    @Test
    public void testInsert() throws Exception {
        Role role = new Role();
        role.getUser().setUserId(3L);
        role.setRoleName("somebody");
        Assert.assertEquals(roleDAO.getById(roleDAO.insert(role)), role);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testUpdate() throws Exception {
        roleObject.setRoleName("somebody");
        roleDAO.update(roleObject);
        Assert.assertEquals(roleDAO.getById(1L), roleObject);
        roleObject.setRoleName("admin");
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testDelete() throws Exception {
        roleDAO.delete(1L);
        Assert.assertEquals(roleDAO.getById(1L), null);
    }

    @DatabaseSetup(value = "classpath:dataset.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
    @Test
    public void testGetByUserId() throws Exception {
        assertThat(roleDAO.getByUserId(roleObject.getUser().getUserId())).contains(roleObject);
    }
}