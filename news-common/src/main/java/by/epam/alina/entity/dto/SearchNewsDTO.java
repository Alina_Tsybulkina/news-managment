package by.epam.alina.entity.dto;

import by.epam.alina.entity.Author;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Data transfer object that include author of news message and set of tags. Used for searching news.
 */
public class SearchNewsDTO {

    private Set<Long> tagsId;

    @NotNull
    private Author author;

    public Set<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(Set<Long> tags) {
        this.tagsId = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchNewsDTO that = (SearchNewsDTO) o;

        if (tagsId != null ? !tagsId.equals(that.tagsId) : that.tagsId != null) return false;
        return author != null ? author.equals(that.author) : that.author == null;

    }

    @Override
    public int hashCode() {
        int result = tagsId != null ? tagsId.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "by.epam.alina.entity.dto.SearchNewsDTO{" +
                "tags=" + tagsId +
                ", author=" + author +
                '}';
    }
}
