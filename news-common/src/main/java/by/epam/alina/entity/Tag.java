package by.epam.alina.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * Entity that describes news tag.
 */
public class Tag implements Serializable {

    private Long tagId;

    @NotNull
    @Size(min = 2, max = 30)
    private String tagName;

    private Set<News> newsSet;

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {
        this.newsSet = newsSet;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "tagId: " + tagId + ", tagName: " + tagName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tag tag = (Tag) obj;

        if (tagId != tag.tagId) {
            return false;
        }

        if (null == tagName) {
            if (tagName != tag.tagName) {
                return false;
            }
        } else {
            if (!tagName.equals(tag.tagName))
                return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return 31 * (int) (tagId + (tagName == null ? 0 : tagName.hashCode()));
    }


}