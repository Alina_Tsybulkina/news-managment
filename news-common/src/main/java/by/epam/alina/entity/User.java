package by.epam.alina.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity that describes user.
 */
public class User implements Serializable, UserDetails{

    private Long userId;
    private String userName;
    private String login;
    private String password;
    private Set<Role> roles = new HashSet<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "userId: " + userId + ", userName: " + userName + ", login: "
                + login + ", password: " + password + ", roles: " + roles;
    }

    @Override
    public int hashCode() {
        return 31 * (int)(userId +
                (userName == null ? 0 : userName.hashCode()) +
                (login == null ? 0 : login.hashCode()) +
                (password == null ? 0 : password.hashCode()));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        User user = (User) obj;

        if (null == userId) {
            if (userId != user.userId) {
                return false;
            }
        } else {
            if (!userId.equals(user.userId))
                return false;
        }

        if (null == userName) {
            if (userName != user.userName) {
                return false;
            }
        } else {
            if (!userName.equals(user.userName))
                return false;
        }

        if (null == login) {
            if (login != user.login) {
                return false;
            }
        } else {
            if (!login.equals(user.login))
                return false;
        }

        if (null == password) {
            if (password != user.password) {
                return false;
            }
        } else {
            if (!password.equals(user.password))
                return false;
        }
        return true;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}