package by.epam.alina.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity that describes author of news message.
 */
public class Author implements Serializable {

    private Long authorId;

    @NotNull
    @Size(min = 2, max = 30)
    private String authorName;

    private Timestamp expired;

    private Set<News> newsSet = new HashSet<>();

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {
        this.newsSet = newsSet;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "authorId: " + authorId + ", authorName: " + authorName
                + ", expired: " + expired;
    }

    @Override
    public int hashCode() {
        return 31 * (int) (authorId + (null == authorName ? 0 : authorName.hashCode()) +
                (null == expired ? 0 : expired.hashCode()));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Author author = (Author) obj;

        if (author.authorId != authorId) {
            return false;
        }

        if (null == authorName) {
            if (authorName != author.authorName) {
                return false;
            }
        } else if (!author.authorName.equals(authorName)) {
            return false;
        }

        if (null == expired) {
            if (expired != author.expired) {
                return false;
            }
        } else if (!author.expired.equals(expired)) {
            return false;
        }

        return true;
    }
}
