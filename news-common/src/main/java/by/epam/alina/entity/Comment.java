package by.epam.alina.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Entity that describes comment to news message.
 */
public class Comment implements Serializable{

	private Long commentId;

	@NotNull
	private News news = new News();

	@NotNull
	@Size(min = 1, max = 100)
	private String commentText;

	private Timestamp creationDate;

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return getClass().getName() + "@" + "commentId: " + commentId
                        + ", newsId: " + news.getNewsId()
				+ ", commentText: " + commentText + ", creationDate: "
				+ creationDate;
	}

	@Override
	public int hashCode() {
		return 31 * (int)( (commentId == null ? 0 : commentId.longValue()) +
                (news.getNewsId() == null ? 0 : news.getNewsId().hashCode()) +
                (commentText == null ? 0 : commentText.hashCode()) +
                (creationDate == null ? 0 : creationDate.hashCode()));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (null == obj) {
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Comment comment = (Comment) obj;
                
		if (commentId != comment.commentId) {
			return false;
		}
                
		if (null == news.getNewsId()) {
			if (news.getNewsId() != comment.news.getNewsId()) {
				return false;
			}
		} else {
			if (!news.getNewsId().equals(comment.news.getNewsId()))
				return false;
		}
                
		if (null == commentText) {
			if (commentText != comment.commentText) {
			return false;
			}
		} else {
			if (!commentText.equals(comment.commentText))
			return false;
		}
		if (null == creationDate) {
			if (creationDate != comment.creationDate) {
				return false;
			}
		} else {
			if (! creationDate.equals(comment.creationDate)) {
				return false;
			}
		}
		return true;
	}
}
