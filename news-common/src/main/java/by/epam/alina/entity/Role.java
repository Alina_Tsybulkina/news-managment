package by.epam.alina.entity;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
/**
 * Entity that describes role of user.
 */
public class Role implements Serializable, GrantedAuthority {
    
    private User user = new User();
    
    private Long roleId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    
    @NotNull
    @Size(min = 2, max = 30)
    private String roleName;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String getAuthority() {
        return roleName;
    }

    public void setAuthority(String roleName) {
        this.roleName = roleName;
    }

    @Override
	public String toString() {
		return getClass().getName() + "@" + "userId: " + user.getUserId() +
                ", roleId: " + roleId +
                ", roleName: " + roleName;
	}

    @Override
    public int hashCode() {
        return (int) (31 * ( (user.getUserId() == null ? 0 : user.getUserId()) +
                (roleId == null ? 0 : roleId.hashCode()) +
                (roleName == null ? 0 : roleName.hashCode()) ));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        Role role = (Role) obj;

        if (null == user.getUserId()) {
            if (user.getUserId() != role.user.getUserId()) {
                return false;
            }
        } else {
            if (!user.getUserId().equals(role.user.getUserId()))
                return false;
        }
        
        if (null == roleId) {
            if (roleId != role.roleId) {
                return false;
            }
        } else {
            if (!roleId.equals(role.roleId))
                return false;
        }

        if (null == roleName) {
            if (roleName != role.roleName) {
                return false;
            }
        } else {
            if (!roleName.equals(role.roleName))
                return false;
        }

        return true;
    }

}