package by.epam.alina.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity that describes news message.
 */
public class News implements Serializable {

    private Long newsId;

    @NotNull
    @Size(min = 3, max = 30)
    private String title;

    @NotNull
    @Size(min = 3, max = 100)
    private String shortText;

    @NotNull
    @Size(min = 3, max = 2000)
    private String fullText;

    private Timestamp creationDate;
    private Date modificationDate;
    private Set<Comment> comments = new HashSet<>();
    private Set<Tag> tags = new HashSet<>();
    private Set<Author> authors = new HashSet<>();

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "newsId: " + newsId + ", title: " + title
                + ", shortText: " + shortText + ", fullText: " + fullText
                + ", creationDate: " + creationDate + ", modificationDate: "
                + modificationDate;
    }

    @Override
    public int hashCode() {
        return 31 * (int) ((newsId == null ? 0 : newsId.longValue()) +
                (title == null ? 0 : title.hashCode()) +
                (shortText == null ? 0 : shortText.hashCode())
                + (fullText == null ? 0 : fullText.hashCode()) +
                (creationDate == null ? 0 : creationDate.hashCode()) +
                (modificationDate == null ? 0 : modificationDate.hashCode()));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        News news = (News) obj;
        if (newsId != news.newsId) {
            return false;
        }

        if (null == title) {
            if (title != news.title) {
                return false;
            }
        } else if (!title.equals(news.title)) {
            return false;
        }

        if (null == shortText) {
            if (shortText != news.shortText) {
                return false;
            }
        } else if (!shortText.equals(news.shortText)) {
            return false;
        }

        if (null == fullText) {
            if (fullText != news.fullText) {
                return false;
            }
        } else if (!fullText.equals(news.fullText)) {
            return false;
        }

        if (null == creationDate) {
            if (creationDate != news.creationDate) {
                return false;
            }
        } else if (!creationDate.equals(news.creationDate)) {
            return false;
        }

        if (null == modificationDate) {
            if (modificationDate != news.modificationDate) {
                return false;
            }
        } else if (!modificationDate.toString().equals(news.modificationDate.toString())) {
            return false;
        }

        return true;
    }
}
