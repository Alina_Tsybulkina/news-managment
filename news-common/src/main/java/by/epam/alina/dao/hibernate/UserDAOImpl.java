package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.UserDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Transactional
public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(UserDAOImpl.class);

    private static final String HQL_DELETE_USER =
            "delete from User where userId = :userId";

    private static final String HQL_GET_BY_LOGIN =
            "from User where login = :login";

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long insert(User user) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(user);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void update(User user) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(user);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_USER)
                    .setParameter("userId", id).executeUpdate();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public User getById(Long id) throws DaoException {
        try {
            return (User) sessionFactory.getCurrentSession().get(User.class, id);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public User getByLogin(User user) throws DaoException {
        try {
            return (User) sessionFactory.getCurrentSession().createQuery(HQL_GET_BY_LOGIN)
                    .setParameter("login", user.getLogin())
                    .uniqueResult();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Set<User> getAll() throws DaoException {
        try {
            return new HashSet<>(sessionFactory.getCurrentSession()
                    .createCriteria(User.class).list());
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

}


