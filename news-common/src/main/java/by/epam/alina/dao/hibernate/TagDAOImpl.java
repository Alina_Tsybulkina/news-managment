package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.TagDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.News;
import by.epam.alina.entity.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Transactional
public class TagDAOImpl implements TagDAO {


    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(TagDAOImpl.class);

    private static final String HQL_GET_TAG_BY_NAME = "from Tag where tagName = :tagName";
    private static final String HQL_DELETE_TAG = "delete from Tag where tagId = :tagId";

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long insert(Tag tag) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(tag);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void update(Tag tag) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(tag);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_TAG)
                    .setParameter("tagId", id).executeUpdate();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Tag getById(Long id) throws DaoException {
        try {
            return (Tag) sessionFactory.getCurrentSession().get(Tag.class, id);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Tag getByName(Tag tag) throws DaoException {
        try {
            return (Tag) sessionFactory.getCurrentSession()
                    .createQuery(HQL_GET_TAG_BY_NAME)
                    .setParameter("tagName", tag.getTagName()).uniqueResult();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Set<Tag> getByNewsId(Long newsId) throws DaoException {
        try {
            return ((News) sessionFactory.getCurrentSession().get(News.class, newsId))
                    .getTags();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Tag> getAll() throws DaoException {
        try {
            return new HashSet<>(sessionFactory.getCurrentSession()
                    .createCriteria(Tag.class).list());
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

}
