package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Comment;
import by.epam.alina.entity.News;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@inheritDoc}
 */
@Transactional
public class CommentsDAOImpl implements CommentsDAO {

    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(CommentsDAOImpl.class);

    private static final String HQL_DELETE_COMMENT = "delete from Comment where commentId = :comment_id";
    private static final String HQL_GET_BY_ID = "from Comment where commentId = :comment_id";

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long insert(Comment comment) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(comment);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void update(Comment comment) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(comment);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_COMMENT)
                    .setParameter("comment_id", id).executeUpdate();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DaoException {
        try {

        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Comment getById(Long id) throws DaoException {
        try {
            return (Comment) sessionFactory.getCurrentSession().createQuery(HQL_GET_BY_ID)
                    .setParameter("comment_id", id).uniqueResult();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Set<Comment> getByNewsId(Long newsId) throws DaoException {
        try {
            News news = (News) sessionFactory.getCurrentSession()
                    .get(News.class, newsId);
            return news.getComments();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Set<Comment> getAll() throws DaoException {
        try {
            return new HashSet<>(sessionFactory.getCurrentSession().createCriteria(Comment.class)
                    .list());
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }
}
