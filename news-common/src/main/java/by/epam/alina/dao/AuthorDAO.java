package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Author;

import java.util.Set;

public interface AuthorDAO {

    /**
     * Insert new author.
     * @param author object of class {@code Author}.
     * @throws DaoException if insertion was unsuccessful.
     */
    Long insert(Author author) throws DaoException;

    /**
     * Updates author data.
     * @param author object of class {@code Author}.
     * @throws DaoException
     */
    void update(Author author) throws DaoException;

    /**
     * Updates authors expiring date.
     * @param author identifier of author.
     * @throws DaoException if delete was unsuccessful.
     */
    void expire(Author author) throws DaoException;

    /**
     * Deletes author.
     * @param id identifier of author.
     * @throws DaoException if delete was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Getting author by identifier.
     * @param id identifier of author.
     * @return object of class {@code Author}.
     * @throws DaoException if getting was unsuccessful.
     */
    Author getById(Long id) throws DaoException;

    /**
     * Getting author by name.
     * @param author object of class {@code Author}.
     * @return object of class {@code Author}.
     * @throws DaoException if getting was unsuccessful.
     */
    Author getByName(Author author) throws DaoException;

    /**
     * Getting set of authors by news identifier.
     * @param newsId news identifier.
     * @return set of authors.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Author> getByNewsId(Long newsId) throws DaoException;


    /**
     * Getting set of all non-expired authors.
     * @return set of authors.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Author> getAllAvailable() throws DaoException;

    /**
     * Getting set of all authors.
     * @return set of authors.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Author> getAll() throws DaoException;
}
