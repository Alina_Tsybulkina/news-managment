package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.User;
import java.util.Set;

public interface UserDAO {

    /**
     * Inserts new user.
     * @param user object of class {@code User}.
     * @throws DaoException if insertion was unsuccessful.
     */
    Long insert(User user) throws DaoException;

    /**
     * Updates user data.
     * @param user object of class {@code User}.
     * @throws DaoException if update was unsuccessful.
     */
    void update(User user) throws DaoException;

    /**
     * Deletes user.
     * @param id user identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Gets user by identifier.
     * @param id user identifier.
     * @return object of class {@code User}.
     * @throws DaoException if getting was unsuccessful.
     */
    User getById(Long id) throws DaoException;

    /**
     * Gets user by login and password.
     * @param user object of class {@code User}.
     * @return object of class {@code User}.
     * @throws DaoException if getting was unsuccessful.
     */
    User getByLogin(User user) throws DaoException;

    /**
     * Gets all users.
     * @return set of users.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<User> getAll() throws DaoException;
}
