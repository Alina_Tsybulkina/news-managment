package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.News;
import by.epam.alina.entity.dto.SearchNewsDTO;

import java.util.List;
import java.util.Set;

public interface NewsDAO {

    /**
     * Inserts news message with its tags and authors in one transaction.
     * @param news object of class {@code News}.
     * @return news id.
     * @throws DaoException if insertion was unsuccessful.
     */
    Long insert(News news) throws DaoException;

    /**
     * Updates news message.
     * @param news object of class {@code News}.
     * @throws DaoException if update was unsuccessful.
     */
    void update(News news) throws DaoException;

    /**
     * Deletes news message.
     * @param id news identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Gets identifier of previous news message.
     * @param currentId identifier of current news message.
     * @return identifier of previous news message, returns null if there no previous news.
     * @throws DaoException if getting was unsuccessful.
     */
    Long getPrevious(Long currentId) throws DaoException;

    /**
     * Gets identifier of next news message.
     * @param currentId identifier of current news message.
     * @return identifier of next news message, returns null if there no next news.
     * @throws DaoException if getting was unsuccessful.
     */
    Long getNext(Long currentId) throws DaoException;

        /**
         * Gets news message y identifier.
         * @param id news identifier.
         * @return object of class {@code News}.
         * @throws DaoException if getting was unsuccessful.
         */
    News getById(Long id) throws DaoException;

    /**
     * Count all news.
     * @return number of all news.
     * @throws DaoException if count was unsuccessful.
     */
    long getNumberOfNews() throws DaoException;

    /**
     * Gets page of news.
     * @return list of news.
     * @throws DaoException if getting was unsuccessful.
     */
    List<News> getAll(int page) throws DaoException;

    /**
     * Gets list of news.
     * @return list of news.
     * @throws DaoException if getting was unsuccessful.
     */
    List<News> getAll() throws DaoException;

    /**
     * Gets page of sorted news.
     * @return list of news.
     * @throws DaoException if getting was unsuccessful.
     */
    List<News> getMostCommented(int page) throws DaoException;

    /**
     * Searches news by author and tags.
     * @param searchNews data transfer object which contains set of tags and author.
     * @return set o news.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<News> searchNews(SearchNewsDTO searchNews) throws DaoException;
}
