package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Author;
import by.epam.alina.entity.News;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@inheritDoc}
 */
@Transactional
public class AuthorDAOImpl implements AuthorDAO {

    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(AuthorDAOImpl.class);

    private static final String HQL_EXPIRE_AUTHOR
            = "update Author set expired = :expired where authorId = :author_id";

    private static final String HQL_GET_AUTHOR_BY_ID
            = "from Author where authorId = :author_id";

    private static final String HQL_DELETE_AUTHOR
            = "delete from Author where authorId = :author_id";

    private static final String HQL_GET_AUTHOR_BY_NAME
            = "from Author where authorName = :author_name";

    private static final String HQL_SELECT_ALL_AVAILABLE
            = "from Author where expired is null";

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long insert(Author author) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(author);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(Author author) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(author);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }
    
    @Override
    public void expire(Author author) throws DaoException {
        try {
            author.setExpired(new Timestamp(new Date().getTime()));
            sessionFactory.getCurrentSession().update(author);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_AUTHOR)
                    .setParameter("author_id", id)
                    .executeUpdate();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Author getById(Long id) throws DaoException {
        try {
            Author author = (Author) sessionFactory.getCurrentSession()
                    .createQuery(HQL_GET_AUTHOR_BY_ID)
                    .setParameter("author_id", id)
                    .uniqueResult();
            return author;
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Author getByName(Author author) throws DaoException {
        try {
            return (Author) sessionFactory.getCurrentSession()
                    .createQuery(HQL_GET_AUTHOR_BY_NAME)
                    .setParameter("author_name", author.getAuthorName())
                    .uniqueResult();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Set<Author> getByNewsId(Long newsId) throws DaoException {
        try {
            News news = (News) sessionFactory.getCurrentSession()
                    .get(News.class, newsId);
            return news.getAuthors();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Set<Author> getAllAvailable() throws DaoException {
        try {
            return new HashSet<>(sessionFactory.getCurrentSession()
                    .createQuery(HQL_SELECT_ALL_AVAILABLE).list());
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Set<Author> getAll() throws DaoException {
        try {
            Set<Author> authors = new HashSet<>(sessionFactory.getCurrentSession()
                    .createCriteria(Author.class).list());
            return authors;
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }
}
