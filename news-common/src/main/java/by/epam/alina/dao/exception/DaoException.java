package by.epam.alina.dao.exception;

public class DaoException extends Exception {
    private static final long serialVersionUID = 1L;

    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(Exception e)
    {
        super(e);
    }

    public DaoException(String msg, Exception e) {
        super(msg, e);
    }
}
