package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.NewsDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.News;
import by.epam.alina.entity.dto.SearchNewsDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class NewsDAOImpl implements NewsDAO {


    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(NewsDAOImpl.class);

    private static final String HQL_GET_NUMBER_OF_NEWS =
            "select count(*) from News";

    private static final String HQL_DELETE_BY_ID =
            "delete from News where newsId = :news_id";

    private static final String HQL_GET_MOST_COMMENTED =
            "from News as news order by news.comments.size desc, news.newsId asc";

    private static final String HQL_ID_MOST_COMMENTED =
            "select news.newsId from News as news order by news.comments.size desc, news.newsId asc";

    private static final String HQL_SEARCH_NEWS =
            "select news from News as news, in (news.tags) tags, in (news.authors) authors " +
                    "where tags.tagId in :tags and authors.authorId in :author";


    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long insert(News news) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(news);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void update(News news) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(news);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_BY_ID)
                    .setParameter("news_id", id).executeUpdate();
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public News getById(Long id) throws DaoException {
        try {
            return (News) sessionFactory.getCurrentSession().get(News.class, id);
        } catch (HibernateException exception) {
            throw new DaoException(exception);
        }
    }

    @Override
    public Long getPrevious(Long currentId) throws DaoException {
        try {
            List<Long> idList = sessionFactory.getCurrentSession()
                    .createQuery(HQL_ID_MOST_COMMENTED).list();
            if (idList.indexOf(currentId) == 0) {
                return null;
            }
            return idList.get(idList.indexOf(currentId) - 1);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Long getNext(Long currentId) throws DaoException {
        try {
            List<Long> idList = sessionFactory.getCurrentSession()
                    .createQuery(HQL_ID_MOST_COMMENTED).list();
            if (idList.indexOf(currentId) + 1 == idList.size()) {
                return null;
            }
            return idList.get(idList.indexOf(currentId) + 1);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public long getNumberOfNews() throws DaoException {
        try {
            return (long) sessionFactory.getCurrentSession().createQuery(HQL_GET_NUMBER_OF_NEWS).uniqueResult();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<News> getAll(int page) throws DaoException {
        try {
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(News.class);
            criteria.setFirstResult(page * 3 - 2);
            criteria.setMaxResults(3);
            return criteria.list();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<News> getAll() throws DaoException {
        try {
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(News.class);
            return criteria.list();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<News> getMostCommented(int page) throws DaoException {
        try {
            Query query = sessionFactory.getCurrentSession()
                    .createQuery(HQL_GET_MOST_COMMENTED);
            query.setFirstResult((page - 1) * 3);
            query.setMaxResults(3);
            return query.list();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Set<News> searchNews(SearchNewsDTO searchNews) throws DaoException {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(HQL_SEARCH_NEWS);
            query.setParameterList("tags", searchNews.getTagsId());
            query.setParameter("author", searchNews.getAuthor().getAuthorId());
            return new HashSet<>(query.list());
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

}
