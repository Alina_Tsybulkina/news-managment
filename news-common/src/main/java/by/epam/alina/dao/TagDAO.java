package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Tag;
import java.util.Set;

public interface TagDAO {

    /**
     * Inserts new tag.
     *
     * @param tag object of class {@code Tag}.
     * @return tags id.
     * @throws DaoException if insertion was unsuccessful.
     */
    Long insert(Tag tag) throws DaoException;

    /**
     * Updates tag.
     *
     * @param tag object of class {@code Tag}.
     * @throws DaoException if update was unsuccessful.
     */
    void update(Tag tag) throws DaoException;

    /**
     * Deletes tag.
     *
     * @param id tags identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Gets tag by identifier.
     *
     * @param id tags identifier.
     * @return object of class {@code Tag}.
     * @throws DaoException if getting was unsuccessful.
     */
    Tag getById(Long id) throws DaoException;

    /**
     * Gets tag by name.
     *
     * @param tag object of class {@code Tag}.
     * @return object of class {@code Tag}.
     * @throws DaoException if getting was unsuccessful.
     */
    Tag getByName(Tag tag) throws DaoException;

    /**
     * Gets tag by news identifier.
     *
     * @param newsId news identifier.
     * @return set of news tags.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Tag> getByNewsId(Long newsId) throws DaoException;

    /**
     * Gets all tags.
     *
     * @return set of tags.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Tag> getAll() throws DaoException;
}
