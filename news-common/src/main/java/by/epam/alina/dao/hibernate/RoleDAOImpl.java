package by.epam.alina.dao.hibernate;

import by.epam.alina.dao.RoleDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Role;
import by.epam.alina.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional
public class RoleDAOImpl implements RoleDAO {

    private SessionFactory sessionFactory;

    private final static Logger LOGGER = LogManager.getLogger(RoleDAOImpl.class);

    private static final String HQL_DELETE_ROLE = "delete from Role where roleId = :roleId";

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long insert(Role role) throws DaoException {
        try {
            return (Long) sessionFactory.getCurrentSession().save(role);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void update(Role role) throws DaoException {
        try {
            sessionFactory.getCurrentSession().update(role);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Role getById(Long id) throws DaoException {
        try {
            return (Role) sessionFactory.getCurrentSession().get(Role.class, id);
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public void delete(Long id) throws DaoException {
        try {
            sessionFactory.getCurrentSession().createQuery(HQL_DELETE_ROLE)
                    .setParameter("roleId", id).executeUpdate();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

    public Set<Role> getByUserId(Long userId) throws DaoException {
        try {
            return ((User) sessionFactory.getCurrentSession()
                    .get(User.class, userId))
                    .getRoles();
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
    }

}
