package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Role;

import java.util.Set;

public interface RoleDAO {

    /**
     * Inserts new role.
     * @param role object of class {@code Role}.
     * @throws DaoException if adding was unsuccessful.
     */
    Long insert(Role role) throws DaoException;

    /**
     * Updates the role.
     * @param role object of class {@code Role}.
     * @throws DaoException if update was unsuccessful.
     */
    void update(Role role) throws DaoException;

    /**
     * Gets role by id.
     * @param id role identifier.
     * @return
     * @throws DaoException
     */
    Role getById(Long id) throws DaoException;

    /**
     * Deletes role.
     * @param id user identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Gets the role by user identifier.
     * @param userId user identifier.
     * @return object of class {@code Role}.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Role> getByUserId(Long userId) throws DaoException;
}
