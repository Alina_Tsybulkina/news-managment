package by.epam.alina.dao;

import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Comment;
import java.util.Set;


public interface CommentsDAO {

    /**
     * Adds comment.
     * @param comment object of class {@code Comment}.
     * @return comment id.
     * @throws DaoException if adding was unsuccessful.
     */
    Long insert(Comment comment) throws DaoException;

    /**
     * Updates comment.
     * @param comment object of class {@code Comment}.
     * @throws DaoException if updating was unsuccessful.
     */
    void update(Comment comment) throws DaoException;

    /**
     * Deletes comment.
     * @param id comments identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void delete(Long id) throws DaoException;

    /**
     * Deletes all comments for news message.
     * @param newsId news message identifier.
     * @throws DaoException if deleting was unsuccessful.
     */
    void deleteNewsComments(Long newsId) throws DaoException;

    /**
     * Gets comment by identifier.
     * @param id comments identifier.
     * @return object of class {@code Comment}.
     * @throws DaoException if getting was unsuccessful.
     */
    Comment getById(Long id) throws DaoException;

    /**
     * Gets comments by news id.
     * @param newsId news identifier.
     * @return set of comments.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Comment> getByNewsId(Long newsId) throws DaoException;

    /**
     * Gets all comments.
     * @return set of comments.
     * @throws DaoException if getting was unsuccessful.
     */
    Set<Comment> getAll() throws DaoException;
}
