package by.epam.alina.service;

import by.epam.alina.entity.Role;
import by.epam.alina.service.exception.ServiceException;

import java.util.Set;

public interface RoleService {

    /**
     * Inserts new role.
     * @param role object of class {@code Role}.
     * @throws ServiceException if adding was unsuccessful.
     */
    void insert(Role role) throws ServiceException;

    /**
     * Updates the role.
     * @param role object of class {@code Role}.
     * @throws ServiceException if update was unsuccessful.
     */
    void update(Role role) throws ServiceException;

    /**
     * Deletes role.
     * @param id user identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Gets the role by user identifier.
     * @param userId user identifier.
     * @return object of class {@code Role}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Role> getByUserId(Long userId) throws ServiceException;
}
