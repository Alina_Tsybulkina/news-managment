package by.epam.alina.service.impl;

import by.epam.alina.dao.RoleDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Role;
import by.epam.alina.service.RoleService;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public void insert(Role role) throws ServiceException {
        try{
            roleDAO.insert(role);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Role role) throws ServiceException {
        try{
            roleDAO.update(role);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(Long id) throws ServiceException {
        try{
            roleDAO.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Set<Role> getByUserId(Long userId) throws ServiceException {
        Set<Role> roles;
        try{
            roles = roleDAO.getByUserId(userId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return roles;
    }
}
