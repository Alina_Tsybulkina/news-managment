package by.epam.alina.service;

import by.epam.alina.entity.Tag;
import by.epam.alina.service.exception.ServiceException;

import java.util.Set;

public interface TagService {

    /**
     * Inserts new tag.
     * @param tag object of class {@code Tag}.
     * @throws ServiceException if insertion was unsuccessful.
     */
    void insert(Tag tag) throws ServiceException;

    /**
     * Updates tag.
     * @param tag object of class {@code Tag}.
     * @throws ServiceException if update was unsuccessful.
     */
    void update(Tag tag) throws ServiceException;

    /**
     * Deletes tag.
     * @param id tags identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Gets tag by identifier.
     * @param id tags identifier.
     * @return object of class {@code Tag}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Tag getById(Long id) throws ServiceException;

    /**
     * Gets tag by name.
     * @param tag object of class {@code Tag}.
     * @return object of class {@code Tag}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Tag getByName(Tag tag) throws ServiceException;

    /**
     * Gets tag by news identifier.
     * @param newsId news identifier.
     * @return set of news tags.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Tag> getByNewsId(Long newsId) throws ServiceException;

    /**
     * Gets all tags.
     * @return set of tags.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Tag> getAll() throws ServiceException;
}
