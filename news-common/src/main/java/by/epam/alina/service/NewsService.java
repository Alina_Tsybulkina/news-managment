package by.epam.alina.service;

import by.epam.alina.entity.News;
import by.epam.alina.entity.dto.SearchNewsDTO;
import by.epam.alina.service.exception.ServiceException;

import java.util.List;
import java.util.Set;

public interface NewsService {

    /**
     * Inserts news message with its tags and authors in one transaction.
     * @param news object of class {@code News}.
     * @throws ServiceException if insertion was unsuccessful.
     */
    void insertNews(News news) throws ServiceException;

    /**
     * Updates news message.
     * @param news object of class {@code News}.
     * @throws ServiceException if update was unsuccessful.
     */
    void updateNews(News news) throws ServiceException;

    /**
     * Deletes news message.
     * @param newsId news identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * Gets all news.
     * @return set of all news.
     * @throws ServiceException if getting was unsuccessful.
     */
    List<News> getAllNews() throws ServiceException;

    /**
     * Gets list of news sorted by most commented news.
     * @return list of news with its authors, tags and comments.
     * @throws ServiceException if getting was unsuccessful.
     * @param page
     */
    List<News> getSortedNews(int page) throws ServiceException;

    /**
     * Gets identifier of previous news message.
     * @param currentId identifier of current news message.
     * @return identifier of previous news message, returns null if there no previous news.
     * @throws ServiceException if getting was unsuccessful.
     */
    Long getPrevious(Long currentId) throws ServiceException;

    /**
     * Gets identifier of next news message.
     * @param currentId identifier of current news message.
     * @return identifier of next news message, returns null if there no next news.
     * @throws ServiceException if getting was unsuccessful.
     */
    Long getNext(Long currentId) throws ServiceException;

    /**
     * Gets news message y identifier.
     * @param newsId news identifier.
     * @return object of class {@code News}.
     * @throws ServiceException if getting was unsuccessful.
     */
    News getOne(Long newsId) throws ServiceException;

    /**
     * Count all news.
     * @return number of all news.
     * @throws ServiceException if count was unsuccessful.
     */
    long countNews() throws ServiceException;

    /**
     * Searches news by author and tags.
     * @param searchNewsDTO data transfer object which contains set of tags and author.
     * @return set o news.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<News> searchNews(SearchNewsDTO searchNewsDTO) throws ServiceException;

}
