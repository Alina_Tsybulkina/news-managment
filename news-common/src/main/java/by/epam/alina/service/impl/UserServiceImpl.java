package by.epam.alina.service.impl;

import by.epam.alina.dao.RoleDAO;
import by.epam.alina.dao.UserDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.User;
import by.epam.alina.service.UserService;
import by.epam.alina.service.exception.ServiceException;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    public void insert(User user) throws ServiceException {
        try{
            userDAO.insert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void update(User user) throws ServiceException {
        try{
            userDAO.update(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(Long id) throws ServiceException {
        try{
            userDAO.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public User getById(Long id) throws ServiceException {
        User user;
        try{
            user = userDAO.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return user;
    }


    public Set<User> getAll() throws ServiceException {
        Set<User> users;
        try{
            users = userDAO.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return users;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException, DataAccessException {
        User user = new User();
        user.setLogin(login);
        try{
            user = userDAO.getByLogin(user);
            System.out.println("\n" + user);
            if (user != null) {
                return user;
            } else {
                throw new UsernameNotFoundException("User not found");
            }
        } catch (DaoException e) {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
