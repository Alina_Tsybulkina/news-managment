package by.epam.alina.service;

import by.epam.alina.entity.Author;
import by.epam.alina.service.exception.ServiceException;

import java.util.Set;

public interface AuthorService {

    /**
     * Insert new author.
     * @param author object of class {@code Author}.
     * @throws ServiceException if insertion was unsuccessful.
     */
    void insert(Author author) throws ServiceException;

    /**
     * Updates author data.
     * @param author object of class {@code Author}.
     * @throws ServiceException if update was unsuccessful.
     */
    void update(Author author) throws ServiceException;

    /**
     * Deletes author.
     * @param id identifier of author.
     * @throws ServiceException if delete was unsuccessful.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Expires author.
     * @param author identifier of author.
     * @throws ServiceException if delete was unsuccessful.
     */
    void expire(Author author) throws ServiceException;

    /**
     * Getting author by identifier.
     * @param id identifier of author.
     * @return object of class {@code Author}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Author getById(Long id) throws ServiceException;

    /**
     * Getting author by name.
     * @param author object of class {@code Author}.
     * @return object of class {@code Author}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Author getByName(Author author) throws ServiceException;

    /**
     * Getting set of authors by news identifier.
     * @param newsId news identifier.
     * @return set of authors.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Author> getByNewsId(Long newsId) throws ServiceException;

    /**
     * Getting set of all non-expired authors.
     * @return set of authors.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Author> getAllAvailable() throws ServiceException;

    /**
     * Getting set of all authors.
     * @return set of authors.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Author> getAll() throws ServiceException;
}
