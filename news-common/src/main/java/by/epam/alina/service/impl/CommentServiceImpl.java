package by.epam.alina.service.impl;

import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Comment;
import by.epam.alina.service.CommentService;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentsDAO commentsDAO;

    @Override
    public void addComment(Comment comment) throws ServiceException {
        try{
            commentsDAO.insert(comment);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateComment(Comment comment) throws ServiceException {
        try{
            commentsDAO.update(comment);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try{
            commentsDAO.delete(commentId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceException {
        try {
            commentsDAO.deleteNewsComments(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment getById(Long commentId) throws ServiceException {
        Comment comment;
        try{
            comment = commentsDAO.getById(commentId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return comment;
    }

    @Override
    public Set<Comment> getByNewsId(Long newsId) throws ServiceException {
        Set<Comment> comments;
        try{
            comments = commentsDAO.getByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return comments;
    }

    @Override
    public Set<Comment> getAll() throws ServiceException {
        Set<Comment> comments;
        try{
            comments = commentsDAO.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return comments;
    }
}
