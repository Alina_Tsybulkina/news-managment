package by.epam.alina.service;

import by.epam.alina.entity.User;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Set;

public interface UserService extends UserDetailsService {

    /**
     * Inserts new user.
     * @param user object of class {@code User}.
     * @throws ServiceException if insertion was unsuccessful.
     */
    void insert(User user) throws ServiceException;

    /**
     * Updates user data.
     * @param user object of class {@code User}.
     * @throws ServiceException if update was unsuccessful.
     */
    void update(User user) throws ServiceException;

    /**
     * Deletes user.
     * @param id user identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Gets user by identifier.
     * @param id user identifier.
     * @return object of class {@code User}.
     * @throws ServiceException if getting was unsuccessful.
     */
    User getById(Long id) throws ServiceException;

    /**
     * Gets all users.
     * @return set of users.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<User> getAll() throws ServiceException;
}
