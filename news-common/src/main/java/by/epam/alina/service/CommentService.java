package by.epam.alina.service;

import by.epam.alina.entity.Comment;
import by.epam.alina.service.exception.ServiceException;
import java.util.Set;

public interface CommentService {

    /**
     * Adds comment.
     * @param comment object of class {@code Comment}.
     * @throws ServiceException if adding was unsuccessful.
     */
    void addComment(Comment comment) throws ServiceException;

    /**
     * Updates comment.
     * @param comment object of class {@code Comment}.
     * @throws ServiceException if updating was unsuccessful.
     */
    void updateComment(Comment comment) throws ServiceException;

    /**
     * Deletes comment.
     * @param commentId comments identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void deleteComment(Long commentId) throws ServiceException;

    /**
     * Deletes all comments for news message.
     * @param newsId news message identifier.
     * @throws ServiceException if deleting was unsuccessful.
     */
    void deleteNewsComments(Long newsId) throws ServiceException;

    /**
     * Gets comment by identifier.
     * @param commentId comments identifier.
     * @return object of class {@code Comment}.
     * @throws ServiceException if getting was unsuccessful.
     */
    Comment getById(Long commentId) throws ServiceException;

    /**
     * Gets comments by news id.
     * @param newsId news identifier.
     * @return set of comments.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Comment> getByNewsId(Long newsId) throws ServiceException;

    /**
     * Gets all comments.
     * @return set of comments.
     * @throws ServiceException if getting was unsuccessful.
     */
    Set<Comment> getAll() throws ServiceException;
}
