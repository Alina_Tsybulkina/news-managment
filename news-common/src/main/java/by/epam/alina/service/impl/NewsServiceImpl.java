package by.epam.alina.service.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.dao.CommentsDAO;
import by.epam.alina.dao.NewsDAO;
import by.epam.alina.dao.TagDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.News;
import by.epam.alina.entity.dto.SearchNewsDTO;
import by.epam.alina.service.NewsService;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private CommentsDAO commentsDAO;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public void insertNews(News news) throws ServiceException {
        try {
            news.setNewsId(newsDAO.insert(news));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> getAllNews() throws ServiceException {
        List<News> newsSet;
        try {
            newsSet = newsDAO.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return newsSet;
    }

    public List<News> getSortedNews(int page) throws ServiceException {
        List<News> newsList;
        try {
            newsList = newsDAO.getMostCommented(page);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return newsList;
    }

    @Override
    public Long getPrevious(Long currentId) throws ServiceException {
        try {
            return newsDAO.getPrevious(currentId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long getNext(Long currentId) throws ServiceException {
        try {
            return newsDAO.getNext(currentId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public News getOne(Long newsId) throws ServiceException {
        News news;
        try {
            news = newsDAO.getById(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    public long countNews() throws ServiceException {
        try {
            return newsDAO.getNumberOfNews();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Set<News> searchNews(SearchNewsDTO searchNews) throws ServiceException {
        try {
            return newsDAO.searchNews(searchNews);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
