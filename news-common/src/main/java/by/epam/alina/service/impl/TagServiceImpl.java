package by.epam.alina.service.impl;

import by.epam.alina.dao.TagDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Tag;
import by.epam.alina.service.TagService;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    public void insert(Tag tag) throws ServiceException {
        try{
            tagDAO.insert(tag);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Tag tag) throws ServiceException {
        try{
            tagDAO.update(tag);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(Long id) throws ServiceException {
        try{
            tagDAO.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Tag getById(Long id) throws ServiceException {
        Tag tag;
        try{
            tag = tagDAO.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return tag;
    }

    public Tag getByName(Tag tag) throws ServiceException {
        try{
            tag = tagDAO.getByName(tag);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return tag;
    }

    public Set<Tag> getByNewsId(Long newsId) throws ServiceException {
        Set<Tag> tags;
        try{
            tags = tagDAO.getByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return tags;
    }

    @Override
    public Set<Tag> getAll() throws ServiceException {
        Set<Tag> tags;
        try{
            tags = tagDAO.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return tags;
    }
}
