package by.epam.alina.service.impl;

import by.epam.alina.dao.AuthorDAO;
import by.epam.alina.dao.exception.DaoException;
import by.epam.alina.entity.Author;
import by.epam.alina.service.AuthorService;
import by.epam.alina.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDAO authorDAO;

    public void insert(Author author) throws ServiceException {
        try{
            authorDAO.insert(author);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Author author) throws ServiceException {
        try{
            authorDAO.update(author);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(Long id) throws ServiceException {
        try{
            authorDAO.delete(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void expire(Author author) throws ServiceException {
        try{
            authorDAO.expire(author);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Author getById(Long id) throws ServiceException {
        Author author;
        try{
            author = authorDAO.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return author;
    }

    public Author getByName(Author author) throws ServiceException {
        try{
            author = authorDAO.getByName(author);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return author;
    }

    public Set<Author> getByNewsId(Long newsId) throws ServiceException {
        Set<Author> authors;
        try{
            authors = authorDAO.getByNewsId(newsId);
        }
        catch (DaoException e){
            throw new ServiceException(e);
        }
        return authors;
    }

    @Override
    public Set<Author> getAllAvailable() throws ServiceException {
        Set<Author> authors;
        try{
            authors = authorDAO.getAllAvailable();
        }
        catch (DaoException e){
            throw new ServiceException(e);
        }
        return authors;
    }

    public Set<Author> getAll() throws ServiceException {
        Set<Author> authors;
        try{
            authors = authorDAO.getAll();
        }
        catch (DaoException e){
            throw new ServiceException(e);
        }
        return authors;
    }
}
